package com.atlassian.upm.api.log;

import java.util.Set;

/**
 * Provides a log of all events that change the state of the plugin system. An event is stored as a {@link AuditLogEntry}
 * which consists of a date, user name, and message. The log can be retrieved as a collection of events.
 * <p/>
 * Implementations should be thread safe. Multiple threads may assume they can call the interface methods
 * safely, assuming no additional atomicity requirements are required between calls. Atomicity may be
 * achieved by synchronizing on the service object and performing successive operations in that block.
 *
 * @since 1.6
 */
public interface PluginLogService
{
    /**
     * Returns all plugin log entries.
     *
     * @return all plugin log entries.
     */
    Iterable<AuditLogEntry> getLogEntries();

    /**
     * Returns {@code maxResults} number of plugin log entries, starting at {@code startIndex}.
     *
     * @param maxResults the maximum number of plugin log entries to return, or "all" if null
     * @param startIndex the starting index of plugin log entries, or 0 if null
     * @return  {@code maxResults} number of plugin log entries, starting at {@code startIndex}.
     */
    Iterable<AuditLogEntry> getLogEntries(Integer maxResults, Integer startIndex);

    /**
     * Returns all plugin log entries with the specified {@link EntryType}s.
     *
     * @param entryTypes the {@link EntryType}s
     * @return all plugin log entries with the specified {@link EntryType}s.
     */
    Iterable<AuditLogEntry> getLogEntries(Set<EntryType> entryTypes);

    /**
     * Returns {@code maxResults} number of plugin log entries of the specified types, starting at {@code startIndex}.
     *
     * @param maxResults the maximum number of plugin log entries to return, or "all" if null
     * @param startIndex the starting index of plugin log entries, or 0 if null
     * @param entryTypes the {@link EntryType}s
     * @return  {@code maxResults} number of plugin log entries, starting at {@code startIndex}.
     */
    Iterable<AuditLogEntry> getLogEntries(Integer maxResults, Integer startIndex, Set<EntryType> entryTypes);
}
