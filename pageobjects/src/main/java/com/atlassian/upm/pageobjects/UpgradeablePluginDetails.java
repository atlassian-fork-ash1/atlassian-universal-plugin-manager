package com.atlassian.upm.pageobjects;

import javax.inject.Inject;

import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.webdriver.AtlassianWebDriver;

import com.google.common.base.Supplier;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import static com.atlassian.upm.pageobjects.Waits.and;
import static com.atlassian.upm.pageobjects.Waits.elementIsVisible;
import static com.atlassian.upm.pageobjects.Waits.loaded;
import static com.atlassian.upm.pageobjects.WebElements.MESSAGE;
import static com.atlassian.upm.pageobjects.WebElements.UPM_DETAILS;
import static com.google.common.base.Preconditions.checkNotNull;

public class UpgradeablePluginDetails
{
    private static final By PLUGIN_DETAILS_LINK = By.cssSelector("a.upm-details-link");

    private @Inject AtlassianWebDriver driver;
    private final UpgradeablePlugin plugin;
    private final Supplier<WebElement> pluginDetails;

    public UpgradeablePluginDetails(Supplier<WebElement> pluginDetails, UpgradeablePlugin plugin)
    {
        this.plugin = checkNotNull(plugin, "plugin");
        this.pluginDetails = checkNotNull(pluginDetails, "pluginDetails");
    }

    @WaitUntil
    public void waitUntilPluginDetailsAreLoaded()
    {
        driver.waitUntil(and(elementIsVisible(UPM_DETAILS, plugin.getWebElement()),
                             loaded(pluginDetails.get())));
    }

    public boolean hasPluginDetailsLink()
    {
        return driver.elementIsVisibleAt(PLUGIN_DETAILS_LINK, pluginDetails.get());
    }

    public WebElement getUpgradeButton()
    {
        return pluginDetails.get().findElement(By.className("upm-upgrade"));
    }

    public void upgrade()
    {
        getUpgradeButton().click();
        driver.waitUntilElementIsVisibleAt(MESSAGE, pluginDetails.get());
    }

    public String getMessage()
    {
        try
        {
            return pluginDetails.get().findElement(MESSAGE).getText();
        }
        catch (NoSuchElementException e)
        {
            return "";
        }
    }
}
