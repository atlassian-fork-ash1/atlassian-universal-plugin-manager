package com.atlassian.upm.pageobjects;

import javax.inject.Inject;

import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.webdriver.AtlassianWebDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.atlassian.upm.pageobjects.Predicates.whereText;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.any;
import static com.google.common.collect.Iterables.find;

public class ConfirmDialog
{
    private static final String CONFIRM_DIALOG_ID = "upm-confirm-dialog";
    private static final By CONFIRM_DIALOG = By.id(CONFIRM_DIALOG_ID);

    private static final String CONTINUE = "Continue";
    private static final String BUTTON = "button";

    private @Inject AtlassianWebDriver driver;

    @FindBy(id=CONFIRM_DIALOG_ID)
    private WebElement dialog;

    @WaitUntil
    public void waitUntilDialogIsVisible()
    {
        driver.waitUntilElementIsVisible(CONFIRM_DIALOG);
    }

    public boolean canConfirm()
    {
        return any(dialog.findElements(By.tagName(BUTTON)), whereText(equalTo(CONTINUE))) &&
                driver.elementIsVisible(CONFIRM_DIALOG);
    }

    public void confirm()
    {
        findConfirmButton().click();
        driver.waitUntilElementIsNotVisible(CONFIRM_DIALOG);
    }

    private WebElement findConfirmButton()
    {
        return find(dialog.findElements(By.tagName(BUTTON)), whereText(equalTo(CONTINUE)));
    }
}
