package com.atlassian.upm.pageobjects;

import javax.inject.Inject;

import com.atlassian.webdriver.AtlassianWebDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ChangesRequiringRestart
{
    private static final String RESTART_PREFIX = "upm-restart-message-";

    static final String REQUIRES_RESTART_MESSAGE_ID = "upm-requires-restart-message";

    private static final By REQUIRES_RESTART_LIST = By.id("upm-requires-restart-list");
    private static final String CANCEL_PREFIX = "upm-cancel-requires-restart-";

    private @Inject AtlassianWebDriver driver;

    @FindBy(id=REQUIRES_RESTART_MESSAGE_ID)
    private WebElement restartMessages;

    @FindBy(id="upm-requires-restart-show")
    private WebElement showLink;

    public boolean detailsShowing()
    {
        return driver.elementIsVisible(REQUIRES_RESTART_LIST);
    }

    public ChangesRequiringRestart showDetails()
    {
        if (!detailsShowing())
        {
            showLink.click();
            driver.waitUntilElementIsVisible(REQUIRES_RESTART_LIST);
        }
        return this;
    }

    public void cancelChange(String key)
    {
        showDetails();

        By cancelLink = By.id(CANCEL_PREFIX + key);
        driver.findElement(cancelLink).click();
        driver.waitUntilElementIsNotLocated(cancelLink);
    }

    public WebElement getMessageForPlugin(String key)
    {
        return restartMessages.findElement(By.id(RESTART_PREFIX + key));
    }
}
