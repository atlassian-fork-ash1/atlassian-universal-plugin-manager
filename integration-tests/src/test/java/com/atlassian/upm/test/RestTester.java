package com.atlassian.upm.test;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.upm.osgi.rest.representations.BundleRepresentation;
import com.atlassian.upm.osgi.rest.representations.BundleSummaryRepresentation;
import com.atlassian.upm.osgi.rest.representations.CollectionRepresentation;
import com.atlassian.upm.osgi.rest.representations.PackageSummaryRepresentation;
import com.atlassian.upm.osgi.rest.representations.ServiceSummaryRepresentation;
import com.atlassian.upm.rest.UpmUriBuilder;
import com.atlassian.upm.rest.async.AsynchronousTask;
import com.atlassian.upm.rest.async.AsynchronousTask.Representation;
import com.atlassian.upm.rest.async.CancellableTaskStatus;
import com.atlassian.upm.rest.async.ErrorTaskStatus;
import com.atlassian.upm.rest.representations.AvailablePluginCollectionRepresentation;
import com.atlassian.upm.rest.representations.AvailablePluginRepresentation;
import com.atlassian.upm.rest.representations.ChangesRequiringRestartRepresentation;
import com.atlassian.upm.rest.representations.FeaturedPluginCollectionRepresentation;
import com.atlassian.upm.rest.representations.FeedProvider;
import com.atlassian.upm.rest.representations.InstalledPluginCollectionRepresentation;
import com.atlassian.upm.rest.representations.JsonProvider;
import com.atlassian.upm.rest.representations.PluginModuleRepresentation;
import com.atlassian.upm.rest.representations.PluginRepresentation;
import com.atlassian.upm.rest.representations.PopularPluginCollectionRepresentation;
import com.atlassian.upm.rest.representations.ProductUpgradePluginCompatibilityRepresentation;
import com.atlassian.upm.rest.representations.ProductUpgradesRepresentation;
import com.atlassian.upm.rest.representations.ProductVersionRepresentation;
import com.atlassian.upm.rest.representations.UpgradesRepresentation;
import com.atlassian.upm.rest.resources.AuditLogSyndicationResource;
import com.atlassian.upm.rest.resources.AuditLogSyndicationResource.FillEntriesRepresentation;
import com.atlassian.upm.rest.resources.InstalledPluginCollectionResource;
import com.atlassian.upm.rest.resources.SafeModeResource.SafeModeFlag;
import com.atlassian.upm.spi.Permission;
import com.atlassian.upm.test.TestRepresentationBuilder.PluginModuleRepresentationBuilder;
import com.atlassian.upm.test.TestRepresentationBuilder.PluginRepresentationBuilder;
import com.atlassian.upm.test.rest.resources.BuildNumberResource;
import com.atlassian.upm.test.rest.resources.PacBaseUrlResource;

import com.atlassian.upm.test.rest.resources.PacModeResource;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.uri.UriBuilderImpl;
import com.sun.jersey.multipart.FormDataMultiPart;
import com.sun.syndication.feed.atom.Entry;
import com.sun.syndication.feed.atom.Feed;

import org.codehaus.jettison.json.JSONException;

import static com.atlassian.integrationtesting.ApplicationPropertiesImpl.getStandardApplicationProperties;
import static com.atlassian.upm.rest.MediaTypes.*;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;
import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Lists.transform;
import static java.util.Arrays.asList;
import static javax.ws.rs.core.MediaType.APPLICATION_ATOM_XML;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM_TYPE;
import static javax.ws.rs.core.MediaType.MULTIPART_FORM_DATA_TYPE;
import static javax.ws.rs.core.Response.Status.ACCEPTED;
import static javax.ws.rs.core.Response.Status.CONFLICT;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.Status.OK;
import static javax.ws.rs.core.Response.Status.SEE_OTHER;

public final class RestTester
{
    private final ApplicationProperties applicationProperties;
    protected final UpmUriBuilder uriBuilder;
    protected final Client client;
    private static final String DEFAULT_USERNAME = "admin";
    private static final String DEFAULT_PASSWORD = "admin";

    public RestTester()
    {
        this(getStandardApplicationProperties(), DEFAULT_USERNAME, DEFAULT_PASSWORD);
    }

    public RestTester(final String username, final String password)
    {
        this(getStandardApplicationProperties(), username, password);
    }

    public RestTester(final ApplicationProperties applicationProperties, final String username, final String password)
    {
        this.applicationProperties = applicationProperties;
        this.uriBuilder = new UpmUriBuilder(applicationProperties)
        {
            @Override
            protected UriBuilder newBaseUriBuilder()
            {
                return new UriBuilderImpl().replacePath(applicationProperties.getBaseUrl()).path("/rest/plugins/1.0");
            }
        };

        ClientConfig config = new DefaultClientConfig();
        config.getClasses().add(JsonProvider.class);
        config.getClasses().add(FeedProvider.class);
        client = Client.create(config);
        client.addFilter(new BasicAuthFilter(password, username));
        client.setFollowRedirects(false);
    }

    public void destroy()
    {
        client.destroy();
    }

    public boolean isSafeMode()
    {
        WebResource safeModeResource = client.resource(uriBuilder.buildSafeModeUri());

        SafeModeFlag flag = safeModeResource.accept(SAFE_MODE_FLAG_JSON).get(SafeModeFlag.class);

        return flag.isEnabled();
    }

    public ClientResponse enterSafeMode()
    {
        WebResource safeModeResource = client.resource(uriBuilder.buildSafeModeUri());

        ClientResponse response = safeModeResource
            .type(SAFE_MODE_FLAG_JSON)
            .accept(SAFE_MODE_FLAG_JSON)
            .put(ClientResponse.class, new SafeModeFlag(true, ImmutableMap.<String, URI>of()));
        // CONFLICT would be returned if already in safe mode
        checkState(response.getResponseStatus().equals(OK) || response.getResponseStatus().equals(CONFLICT), "Entering safe mode returned a " + response.getStatus() +
            " and posted an Audit Log message of '" + getMostRecentAuditLogMessage() + "'");
        return response;
    }

    public ClientResponse exitSafeModeAndKeepState()
    {
        WebResource safeModeResource = client.resource(uriBuilder.buildExitSafeModeUri(true));

        ClientResponse response = safeModeResource
            .type(SAFE_MODE_FLAG_JSON)
            .accept(SAFE_MODE_FLAG_JSON)
            .put(ClientResponse.class, new SafeModeFlag(false, ImmutableMap.<String, URI>of()));
        // CONFLICT would be returned if already not in safe mode
        checkState(response.getResponseStatus().equals(OK) || response.getResponseStatus().equals(CONFLICT), "Exiting safe mode returned a " + response.getStatus() +
            " and posted an Audit Log message of '" + getMostRecentAuditLogMessage() + "'");
        return response;
    }

    public ClientResponse exitSafeModeAndRestoreSavedConfiguration()
    {
        WebResource safeModeResource = client.resource(uriBuilder.buildExitSafeModeUri(false));

        ClientResponse response = safeModeResource
            .type(SAFE_MODE_FLAG_JSON)
            .accept(SAFE_MODE_FLAG_JSON)
            .put(ClientResponse.class, new SafeModeFlag(false, ImmutableMap.<String, URI>of()));
        // CONFLICT would be returned if already not in safe mode
        checkState(response.getResponseStatus().equals(OK) || response.getResponseStatus().equals(CONFLICT), "Exiting safe mode returned a " + response.getStatus() +
            " and posted an Audit Log message of '" + getMostRecentAuditLogMessage() + "'");
        return response;
    }

    private String getMostRecentAuditLogMessage()
    {
        Feed feed = getAuditLogFeed(0, 1);
        List<Entry> entries = (List<Entry>) feed.getEntries();
        return entries.get(0).getTitle();
    }

    public Feed getAuditLogFeed()
    {
        WebResource feedResource = client.resource(uriBuilder.buildAuditLogFeedUri());

        return feedResource.accept(APPLICATION_ATOM_XML).get(Feed.class);
    }

    public Feed getAuditLogFeed(int start, int max)
    {
        WebResource feedResource = client.resource(uriBuilder.buildAuditLogFeedUri());

        return feedResource
            .queryParam("max-results", Integer.toString(max))
            .queryParam("start-index", Integer.toString(start))
            .accept(APPLICATION_ATOM_XML)
            .get(Feed.class);
    }

    public ClientResponse setAuditLogMaxEntries(int maxEntries)
    {
        WebResource maxEntriesResource = client.resource(uriBuilder.buildAuditLogMaxEntriesUri());

        return maxEntriesResource
            .type(AUDIT_LOG_MAX_ENTRIES_JSON)
            .accept(AUDIT_LOG_MAX_ENTRIES_JSON)
            .put(ClientResponse.class, new AuditLogSyndicationResource.MaxEntriesRepresentation(maxEntries));
    }

    public ClientResponse setAuditLogPurgeAfter(int purgeAfter)
    {
        WebResource purgeAfterResource = client.resource(uriBuilder.buildAuditLogPurgeAfterUri());

        return purgeAfterResource
            .type(AUDIT_LOG_PURGE_AFTER_JSON)
            .accept(AUDIT_LOG_PURGE_AFTER_JSON)
            .put(ClientResponse.class, new AuditLogSyndicationResource.PurgeAfterRepresentation(purgeAfter));
    }

    public int getAuditLogMaxEntries()
    {
        WebResource maxEntriesResource = client.resource(uriBuilder.buildAuditLogMaxEntriesUri());

        return maxEntriesResource.accept(AUDIT_LOG_MAX_ENTRIES_JSON).get(AuditLogSyndicationResource.MaxEntriesRepresentation.class).getMaxEntries();
    }

    public int getAuditLogPurgeAfter()
    {
        WebResource purgeAfterResource = client.resource(uriBuilder.buildAuditLogPurgeAfterUri());
        return purgeAfterResource.accept(AUDIT_LOG_PURGE_AFTER_JSON).get(AuditLogSyndicationResource.PurgeAfterRepresentation.class).getPurgeAfter();
    }

    public void fillAuditLogEntries(List<String> entries)
    {
        WebResource addEntriesResource = client.resource(uriBuilder.buildAuditLogFeedUri());
        addEntriesResource
            .type(AUDIT_LOG_ENTRIES_JSON)
            .accept(AUDIT_LOG_ENTRIES_JSON)
            .put(new FillEntriesRepresentation(entries));
    }

    public void purgeAuditLogFeed()
    {
        fillAuditLogEntries(Collections.EMPTY_LIST);
    }

    public String getBuildNumber()
    {
        WebResource buildNumberResource = client.resource(uriBuilder.buildBuildNumberUri());
        return buildNumberResource
            .type(BUILD_NUMBER_JSON)
            .accept(BUILD_NUMBER_JSON)
            .get(BuildNumberResource.BuildNumberRepresentation.class).getBuildNumber();
    }

    public ClientResponse setBuildNumber(String buildNumber)
    {
        WebResource buildNumberResource = client.resource(uriBuilder.buildBuildNumberUri());
        return buildNumberResource
            .type(BUILD_NUMBER_JSON)
            .accept(BUILD_NUMBER_JSON)
            .put(ClientResponse.class, new BuildNumberResource.BuildNumberRepresentation(buildNumber));
    }

    public ClientResponse resetBuildNumber()
    {
        WebResource buildNumberResource = client.resource(uriBuilder.buildBuildNumberUri());
        return buildNumberResource
            .type(BUILD_NUMBER_JSON)
            .accept(BUILD_NUMBER_JSON)
            .delete(ClientResponse.class);
    }

    public boolean getPacMode()
    {
        WebResource pacModeResource = client.resource(uriBuilder.buildPacModeUri());
        return pacModeResource
            .type(PAC_MODE_JSON)
            .accept(PAC_MODE_JSON)
            .get(PacModeResource.PacModeRepresentation.class).isDisabled();
    }

    public ClientResponse setPacMode(boolean offline)
    {
        WebResource pacModeResource = client.resource(uriBuilder.buildPacModeUri());
        return pacModeResource
            .type(PAC_MODE_JSON)
            .accept(PAC_MODE_JSON)
            .put(ClientResponse.class, new PacModeResource.PacModeRepresentation(offline));
    }

    public ClientResponse setPacBaseUrl(String pacBaseUrl)
    {
        WebResource pacBaseUrlResource = client.resource(uriBuilder.buildPacBaseUrlUri());
        return pacBaseUrlResource
            .type(PAC_BASE_URL_JSON)
            .accept(PAC_BASE_URL_JSON)
            .put(ClientResponse.class, new PacBaseUrlResource.PacBaseUrlRepresentation(pacBaseUrl));
    }

    public ClientResponse resetPacBaseUrl()
    {
        WebResource pacBaseUrlResource = client.resource(uriBuilder.buildPacBaseUrlUri());
        return pacBaseUrlResource
            .type(PAC_BASE_URL_JSON)
            .accept(PAC_BASE_URL_JSON)
            .delete(ClientResponse.class);
    }

    public ClientResponse isPacAvailable()
    {
        WebResource pacBaseUrlResource = client.resource(uriBuilder.buildPacBaseUrlUri());
        return pacBaseUrlResource
            .type(PAC_STATUS_JSON)
            .accept(PAC_STATUS_JSON)
            .get(ClientResponse.class);
    }

    /**
     * Used to create a long running task that will continue to run until the {@link #cancelCancellableTask(com.atlassian.upm.rest.async.AsynchronousTask.Representation)}
     * method has been called with the returned representation.
     *
     * @return the representation of the task status
     */
    public Representation<CancellableTaskStatus> createCancellableTask()
    {
        WebResource pendingTaskResource = client.resource(uriBuilder.buildPendingTasksUri());
        final ClientResponse clientResponse = pendingTaskResource.post(ClientResponse.class);
        return clientResponse.getEntity(new GenericType<AsynchronousTask.Representation<CancellableTaskStatus>>()
        {
        });
    }

    /**
     * Cancels a long running task that has been submitted via the {@link #createCancellableTask()} method.
     *
     * @param cancellableTaskStatusRepresentation specifies which task to cancel.
     * @return the response of the task canceling operation.
     */
    public ClientResponse cancelCancellableTask(Representation<CancellableTaskStatus> cancellableTaskStatusRepresentation)
        throws JSONException
    {
        WebResource pendingTaskResource = client.resource(uriBuilder.makeAbsolute(cancellableTaskStatusRepresentation.getSelf()));
        // Cancel the task
        ClientResponse clientResponse = pendingTaskResource.delete(ClientResponse.class);

        // Now lets poll the async task until it is really done, unless something when wrong with the cancel and then
        // we will just return the response.
        if (!clientResponse.getResponseStatus().equals(ACCEPTED))
        {
            return clientResponse;
        }

        return waitForCompletion(clientResponse, ClientResponse.class);
    }

    /**
     * Gets the {@code InstalledPluginCollectionRepresentation} containing all available plugins using the REST service
     *
     * @return the {@code InstalledPluginCollectionRepresentation} containing all available plugins
     */
    public InstalledPluginCollectionRepresentation getInstalledPluginCollection()
    {
        WebResource pluginResource = client.resource(uriBuilder.buildInstalledPluginCollectionUri());
        return pluginResource.accept(INSTALLED_PLUGINS_COLLECTION_JSON).get(InstalledPluginCollectionRepresentation.class);
    }

    public PluginRepresentation getPlugin(TestPlugins plugin)
    {
        return getPlugin(plugin.getKey());
    }

    public <T> T getPlugin(TestPlugins plugin, Class<T> c)
    {
        return getPlugin(plugin.getKey(), c);
    }

    /**
     * Gets the {@code PluginRepresentation} with key {@code pluginKey} using the REST service
     *
     * @param pluginKey the key of the {@code Plugin} to get
     * @return the {@code PluginRepresentation} associated with the {@code pluginKey}
     */
    public PluginRepresentation getPlugin(String pluginKey)
    {
        return getPlugin(pluginKey, PluginRepresentation.class);
    }

    /**
     * Gets the {@code PluginRepresentation} with key {@code pluginKey} using the REST service
     *
     * @param pluginKey the key of the {@code Plugin} to get
     * @return the {@code PluginRepresentation} associated with the {@code pluginKey}
     */
    public <T> T getPlugin(String pluginKey, Class<T> c)
    {
        WebResource pluginResource = client.resource(uriBuilder.buildPluginUri(pluginKey));

        return pluginResource.accept(INSTALLED_PLUGIN_JSON).get(c);
    }

    public PluginModuleRepresentation getPluginModule(TestPlugins plugin, String moduleKey)
    {
        WebResource pluginModuleResource = client.resource(uriBuilder.buildPluginModuleUri(plugin.getKey(), moduleKey));

        return pluginModuleResource.accept(PLUGIN_MODULE_JSON).get(PluginModuleRepresentation.class);
    }

    public ClientResponse enablePlugin(TestPlugins plugin) throws URISyntaxException
    {
        return enablePlugin(plugin.getKey());
    }

    /**
     * Enables a plugin, and returns the {@code ClientResponse} received from the call.
     *
     * @param pluginKey the key of the {@code Plugin} to enable
     * @return the {@code ClientResponse} returned by the REST service call
     */
    public ClientResponse enablePlugin(String pluginKey) throws URISyntaxException
    {
        return enablePlugin(uriBuilder.buildPluginUri(pluginKey));
    }

    /**
     * Enables a plugin from a URI, and returns the {@code ClientResponse} received from the call.
     *
     * @param pluginUri the URI of the {@code Plugin} to enable
     * @return the {@code ClientResponse} returned by the REST service call
     */
    public ClientResponse enablePlugin(URI pluginUri) throws URISyntaxException
    {
        WebResource pluginResource = client.resource(uriBuilder.makeAbsolute(pluginUri));

        return pluginResource
            .type(INSTALLED_PLUGIN_JSON)
            .accept(INSTALLED_PLUGIN_JSON)
            .put(ClientResponse.class, new PluginRepresentationBuilder().build());
    }

    public ClientResponse disablePlugin(TestPlugins plugin) throws URISyntaxException
    {
        return disablePlugin(plugin.getKey());
    }

    /**
     * Disables a plugin, and returns the {@code ClientResponse} received from the call.
     *
     * @param pluginKey the key of the {@code Plugin} to disable
     * @return the {@code ClientResponse} returned by the REST service call
     */
    public ClientResponse disablePlugin(String pluginKey) throws URISyntaxException
    {
        WebResource pluginResource = client.resource(uriBuilder.buildPluginUri(pluginKey));

        return pluginResource
            .type(INSTALLED_PLUGIN_JSON)
            .accept(INSTALLED_PLUGIN_JSON)
            .put(ClientResponse.class, new PluginRepresentationBuilder().enabled(false).build());
    }

    /**
     * Enables a plugin module, and returns the {@code ClientResponse} received from the call.
     *
     * @param pluginKey the ID of the {@code Plugin}
     * @param moduleKey the ID of the {@code ModuleDescriptor} to enable
     * @return the {@code ClientResponse} returned by the REST service call
     */
    public ClientResponse enablePluginModule(String pluginKey, String moduleKey) throws URISyntaxException
    {
        WebResource pluginModuleResource = client.resource(uriBuilder.buildPluginModuleUri(pluginKey, moduleKey));

        return pluginModuleResource
            .type(PLUGIN_MODULE_JSON)
            .accept(PLUGIN_MODULE_JSON)
            .put(ClientResponse.class, new PluginModuleRepresentationBuilder().build());
    }

    /**
     * Disables a plugin module, and returns the {@code ClientResponse} received from the call.
     *
     * @param pluginKey the ID of the {@code Plugin}
     * @param moduleKey the ID of the {@code ModuleDescriptor} to disable
     * @return the {@code ClientResponse} returned by the REST service call
     */
    public ClientResponse disablePluginModule(String pluginKey, String moduleKey) throws URISyntaxException
    {
        WebResource pluginModuleResource = client.resource(uriBuilder.buildPluginModuleUri(pluginKey, moduleKey));

        return pluginModuleResource
            .type(PLUGIN_MODULE_JSON)
            .accept(PLUGIN_MODULE_JSON)
            .put(ClientResponse.class, new PluginModuleRepresentationBuilder().enabled(false).build());
    }

    /**
     * Installs a plugin, and returns the {@code ClientResponse} received from the call.
     *
     * @param plugin {@code TestPlugin} to install
     * @return the {@code ClientResponse} returned by the REST service call
     */
    public ClientResponse installPluginAndWaitForCompletion(TestPlugins plugin) throws JSONException
    {
        return installPluginAndWaitForCompletion(plugin.getDownloadUri(URI.create(applicationProperties.getBaseUrl())));
    }

    /**
     * Installs a plugin, and returns the {@code ClientResponse} received from the call.
     *
     * @param pluginUri the uri of the {@code Plugin} to install
     * @return the {@code ClientResponse} returned by the REST service call
     */
    public ClientResponse installPluginAndWaitForCompletion(URI pluginUri) throws JSONException
    {
        return installPluginAndWaitForCompletion(pluginUri.toASCIIString());
    }

    /**
     * Installs a plugin, and returns the {@code ClientResponse} received from the call.
     *
     * @param pluginUri the uri of the {@code Plugin} to install
     * @return the {@code ClientResponse} returned by the REST service call
     */
    public ClientResponse installPluginAndWaitForCompletion(String pluginUri) throws JSONException
    {
        WebResource pluginCollectionResource = client.resource(uriBuilder.buildInstalledPluginCollectionUri());

        ClientResponse tokenResponse = pluginCollectionResource.head();
        String token = tokenResponse.getMetadata().getFirst("upm-token");
        if (token == null)
        {
            token = "badToken";
        }

        ClientResponse acceptResponse = pluginCollectionResource
            .queryParam("token", token)
            .type(INSTALL_URI_JSON)
            .accept(INSTALL_DOWNLOADING_JSON)
            .post(ClientResponse.class, new InstalledPluginCollectionResource.InstallPluginUri(pluginUri));

        if (!acceptResponse.getResponseStatus().equals(ACCEPTED))
        {
            return acceptResponse;
        }

        return waitForCompletion(acceptResponse, ClientResponse.class);
    }

    /**
     * Install all plugins and verify that they were installed properly.  If they aren't, thrown an IllegalStateException.
     */
    public void installPluginsAndVerify(TestPlugins first, TestPlugins... rest) throws JSONException
    {
        for (TestPlugins plugin : concat(ImmutableList.of(first), asList(rest)))
        {
            installPluginAndWaitForCompletion(plugin);
            checkInstalled(plugin);
        }
    }

    /**
     * Submits an install request, and returns the initial response from the asynchronous task.
     * Does not wait for completion of the asynchronous install task
     *
     * @param pluginUri
     * @return the initial {@code ClientResponse}
     */
    public ClientResponse installPluginAndReturnAcceptedResponse(URI pluginUri)
    {
        return installPluginAndReturnAcceptedResponse(pluginUri.toASCIIString());
    }

    /**
     * Submits an install request, and returns the initial response from the asynchronous task.
     * Does not wait for completion of the asynchronous install task
     *
     * @param pluginUri
     * @return the initial {@code ClientResponse}
     */
    public ClientResponse installPluginAndReturnAcceptedResponse(String pluginUri)
    {
        WebResource pluginCollectionResource = client.resource(uriBuilder.buildInstalledPluginCollectionUri());

        ClientResponse tokenResponse = pluginCollectionResource.head();
        String token = tokenResponse.getMetadata().getFirst("upm-token");

        ClientResponse acceptResponse = pluginCollectionResource
            .queryParam("token", token)
            .type(INSTALL_URI_JSON)
            .accept(INSTALL_DOWNLOADING_JSON)
            .post(ClientResponse.class, new InstalledPluginCollectionResource.InstallPluginUri(pluginUri));

        return acceptResponse;
    }

    public ClientResponse installPluginFromFileSystemAndWaitForCompletion(String pluginUri) throws JSONException
    {
        return installPluginFromFileSystemAndWaitForCompletion(pluginUri, true);
    }

    public ClientResponse installPluginFromFileSystemAndWaitForCompletion(String pluginUri, boolean useToken)
        throws JSONException
    {
        final File pluginFile = new File(this.getClass().getClassLoader().getResource("test-jars/" + pluginUri).getFile());

        final FormDataMultiPart pluginPart = new FormDataMultiPart();
        pluginPart.field("plugin", pluginFile, APPLICATION_OCTET_STREAM_TYPE);

        final MediaType multipartFormDataWithBoundary =
            new MediaType(MULTIPART_FORM_DATA_TYPE.getType(),
                MULTIPART_FORM_DATA_TYPE.getSubtype(),
                ImmutableMap.<String, String>builder().put("boundary", "some simple boundary").build());

        WebResource pluginCollectionResource = client.resource(uriBuilder.buildInstalledPluginCollectionUri());

        String token = "badtoken";
        if (useToken)
        {
            ClientResponse tokenResponse = pluginCollectionResource.head();
            token = tokenResponse.getMetadata().getFirst("upm-token");
        }

        ClientResponse acceptResponse = pluginCollectionResource
            .queryParam("token", token)
            .type(multipartFormDataWithBoundary)
            .post(ClientResponse.class, pluginPart);

        if (acceptResponse.getStatus() != ACCEPTED.getStatusCode())
        {
            return acceptResponse;
        }

        return waitForCompletion(acceptResponse, ClientResponse.class);
    }

    public ClientResponse uninstallPluginAndVerify(TestPlugins plugin)
    {
        return uninstallPluginAndVerify(plugin.getKey());
    }

    /**
     * Uninstalls a plugin, and returns the {@code ClientResponse} received from the call.  Verifies the plugin was
     * installed and if not an {@code IllegalArgumentException} is thrown.
     *
     * @param pluginKey the ID of the {@code Plugin}
     * @return the {@code ClientResponse} returned by the REST service call
     */
    public ClientResponse uninstallPluginAndVerify(String pluginKey)
    {
        WebResource pluginResource = client.resource(uriBuilder.buildPluginUri(pluginKey));

        ClientResponse response = pluginResource
            .type(INSTALLED_PLUGIN_JSON)
            .delete(ClientResponse.class);

        //ensure that the response was either successful or that the plugin didn't exist in the first place
        checkState(asList(OK, NOT_FOUND).contains(response.getResponseStatus()), "Uninstall was " + response.getStatus() + " for plugin " + pluginKey);
        checkNotInstalled(pluginKey);

        return response;
    }

    public ClientResponse uninstallPlugin(TestPlugins plugin)
    {
        return uninstallPlugin(plugin.getKey());
    }

    /**
     * Uninstalls a plugin, and returns the {@code ClientResponse} received from the call.
     *
     * @param pluginKey the ID of the {@code Plugin}
     * @return the {@code ClientResponse} returned by the REST service call
     */
    public ClientResponse uninstallPlugin(String pluginKey)
    {
        WebResource pluginResource = client.resource(uriBuilder.buildPluginUri(pluginKey));

        return pluginResource
            .type(INSTALLED_PLUGIN_JSON)
            .delete(ClientResponse.class);
    }

    /**
     * @return the {@code ProductUpgradesRepresentation} containing versions of the product newer than the current one
     */
    public ProductUpgradesRepresentation getProductUpgrades()
    {
        WebResource productUpgradesResource = client.resource(uriBuilder.buildProductUpgradesUri());
        return productUpgradesResource.accept(PRODUCT_UPGRADES_JSON).get(ProductUpgradesRepresentation.class);
    }

    /**
     * Get the {@code ProductUpgradePluginCompatibilityResource} containing versions of the product newer than the current one
     *
     * @return the {@code ProductUpgradePluginCompatibilityResource} containing versions of the product newer than the current one
     */
    public ProductUpgradePluginCompatibilityRepresentation getProductUpgradePluginCompatibility(Long productUpgradeBuildNumber)
    {
        WebResource productUpgradePluginCompatibilityResource = client.resource(uriBuilder.buildProductUpgradePluginCompatibilityUri(productUpgradeBuildNumber));
        return productUpgradePluginCompatibilityResource.accept(COMPATIBILITY_JSON).get(ProductUpgradePluginCompatibilityRepresentation.class);
    }

    public AvailablePluginCollectionRepresentation searchPlugins(String query)
    {
        return searchPlugins(query, null, null);
    }

    public AvailablePluginCollectionRepresentation searchPlugins(String query, Integer max, Integer offset)
    {
        WebResource plugins = client.resource(uriBuilder.buildAvailablePluginCollectionUri(query, max, offset));

        return plugins
            .type(AVAILABLE_PLUGINS_COLLECTION_JSON)
            .accept(AVAILABLE_PLUGINS_COLLECTION_JSON)
            .get(AvailablePluginCollectionRepresentation.class);
    }

    /**
     * Gets the {@code InstalledPluginCollectionRepresentation} containing popular plugins using the REST service
     *
     * @return the {@code InstalledPluginCollectionRepresentation} containing popular plugins
     */
    public PopularPluginCollectionRepresentation getPopularPluginCollection()
    {
        return getPopularPluginCollection(null, null);
    }

    public PopularPluginCollectionRepresentation getPopularPluginCollection(Integer max, Integer offset)
    {
        WebResource popularPluginCollectionResource = client.resource(uriBuilder.buildPopularPluginCollectionUri(max, offset));
        return popularPluginCollectionResource.accept(POPULAR_PLUGINS_JSON).get(PopularPluginCollectionRepresentation.class);
    }

    public AvailablePluginCollectionRepresentation getAvailablePlugins()
    {
        return getAvailablePlugins(null, null);
    }

    public AvailablePluginCollectionRepresentation getAvailablePlugins(Integer max, Integer offset)
    {
        WebResource availablePlugins = client.resource(uriBuilder.buildAvailablePluginCollectionUri(null, max, offset));

        return availablePlugins
            .type(AVAILABLE_PLUGINS_COLLECTION_JSON)
            .accept(AVAILABLE_PLUGINS_COLLECTION_JSON)
            .get(AvailablePluginCollectionRepresentation.class);
    }

    public <T> T getAvailablePlugins(Class<T> entityType)
    {
        WebResource availablePlugins = client.resource(uriBuilder.buildAvailablePluginCollectionUri(null, null, null));
        return availablePlugins.get(entityType);
    }

    public <T> T getAvailablePlugin(String pluginKey, Class<T> entityType)
    {
        WebResource availablePlugin = client.resource(uriBuilder.buildAvailablePluginUri(pluginKey));

        return availablePlugin
            .type(AVAILABLE_PLUGIN_JSON)
            .accept(AVAILABLE_PLUGIN_JSON)
            .get(entityType);
    }

    public AvailablePluginRepresentation getAvailablePlugin(TestPlugins plugin)
    {
        return getAvailablePlugin(plugin.getKey(), AvailablePluginRepresentation.class);
    }

    public FeaturedPluginCollectionRepresentation getFeaturedPlugins()
    {
        return getFeaturedPlugins(null, null);
    }

    public FeaturedPluginCollectionRepresentation getFeaturedPlugins(Integer max, Integer offset)
    {
        WebResource featuredPlugins = client.resource(uriBuilder.buildFeaturedPluginCollectionUri(max, offset));

        return featuredPlugins
            .type(AVAILABLE_FEATURED_JSON)
            .accept(AVAILABLE_FEATURED_JSON)
            .get(FeaturedPluginCollectionRepresentation.class);
    }

    public <T> T getFeaturedPlugins(Class<T> entityType)
    {
        WebResource featuredPlugins = client.resource(uriBuilder.buildFeaturedPluginCollectionUri(null, null));
        return featuredPlugins.get(entityType);
    }

    public UpgradesRepresentation getUpgrades()
    {
        WebResource upgrades = client.resource(uriBuilder.buildUpgradesUri());
        return upgrades.accept(UPGRADES_JSON).get(UpgradesRepresentation.class);
    }

    public ClientResponse upgradeAll()
    {
        WebResource upgradeAll = client.resource(uriBuilder.buildUpgradeAllUri());
        return upgradeAll.post(ClientResponse.class);
    }

    public ClientResponse upgradeAllAndWaitForCompletion() throws JSONException
    {
        ClientResponse response = upgradeAll();

        if (response.getStatus() != ACCEPTED.getStatusCode())
        {
            return response;
        }

        return waitForCompletion(response, ClientResponse.class);
    }

    public ProductVersionRepresentation getProductVersionRepresentation()
    {
        WebResource productVersion = client.resource(uriBuilder.buildProductVersionUri());
        return productVersion.accept(UPM_JSON).get(ProductVersionRepresentation.class);
    }

    public ChangesRequiringRestartRepresentation getChangesRequiringRestart()
    {
        WebResource changes = client.resource(uriBuilder.buildChangesRequiringRestartUri());

        return changes
            .type(CHANGES_REQUIRING_RESTART_JSON)
            .accept(CHANGES_REQUIRING_RESTART_JSON)
            .get(ChangesRequiringRestartRepresentation.class);
    }

    public void deleteChangeRequiringRestart(TestPlugins testPlugin)
    {
        client.resource(uriBuilder.buildChangeRequiringRestart(testPlugin.getKey())).delete();
    }

    public <T> T waitForCompletion(ClientResponse acceptResponse, Class<T> clazz) throws JSONException
    {
        checkArgument(acceptResponse.getStatus() == ACCEPTED.getStatusCode());

        URI taskLocation = acceptResponse.getLocation();

        WebResource taskResource = client.resource(taskLocation);

        ClientResponse clientResponse = null;
        // set our limit to only 30 iterations before going out of the loop to prevent a possible infinite loop.
        for (int i = 0; i < 30; i++)
        {
            clientResponse = taskResource.accept(PENDING_TASK_JSON).get(ClientResponse.class);
            if (clientResponse.getStatus() != OK.getStatusCode())
            {
                break;
            }
            if (isTaskErrorRepresentation(clientResponse))
            {
                throw new AsynchronousTaskException(getTaskErrorMessage(clientResponse));
            }
            if (isTaskDone(clientResponse))
            {
                break;
            }
            try // sleep for a second, and retry to see if the task has completed
            {
                Thread.sleep(1000);
            }
            catch (InterruptedException ex)
            {
            }
        }

        if (clientResponse.getStatus() == SEE_OTHER.getStatusCode())
        {
            WebResource completionResource = client.resource(clientResponse.getLocation());
            return completionResource.accept(INSTALLED_PLUGIN_JSON).get(clazz);
        }
        else if (!ClientResponse.class.equals(clazz))
        {
            return clientResponse.getEntity(clazz);
        }
        else
        {
            return (T) clientResponse;
        }
    }

    private String getTaskErrorMessage(ClientResponse response) throws JSONException
    {
        checkArgument(isTaskErrorRepresentation(response));
        Representation<ErrorTaskStatus> representation = response.getEntity(new GenericType<Representation<ErrorTaskStatus>>()
        {
        });
        return representation.getStatus().getSubCode();
    }

    private boolean isTaskErrorRepresentation(ClientResponse response)
    {
        return response.getType().getSubtype().endsWith("err+json");
    }

    private boolean isTaskDone(ClientResponse response)
    {
        return response.getType().getSubtype().endsWith("complete+json");
    }

    public UpmUriBuilder getUriBuilder()
    {
        return uriBuilder;
    }

    private final static Function<TestPlugins, String> testPluginToKey = new Function<TestPlugins, String>()
    {
        public String apply(TestPlugins plugin)
        {
            return plugin.getKey();
        }
    };

    public final void checkInstalled(TestPlugins plugin)
    {
        if (getPlugin(plugin.getKey(), ClientResponse.class).getResponseStatus() != OK)
        {
            throw new IllegalStateException("Plugin " + plugin + " is not installed");
        }
    }

    public final void checkNotInstalled(TestPlugins... plugins)
    {
        checkNotInstalled(transform(asList(plugins), testPluginToKey).toArray(new String[plugins.length]));
    }

    public final void checkNotInstalled(String... keys)
    {
        ImmutableList.Builder<String> builder = ImmutableList.builder();
        for (String key : keys)
        {
            if (getPlugin(key, ClientResponse.class).getResponseStatus() != NOT_FOUND)
            {
                builder.add(key);
            }
        }
        List<String> installed = builder.build();
        if (installed.size() == 1)
        {
            throw new IllegalStateException("Plugin " + installed.get(0) + " is installed");
        }
        else if (installed.size() > 1)
        {
            throw new IllegalStateException("Plugins " + installed + " are installed");
        }
    }

    public void ensureNotInstalled(TestPlugins... plugins)
    {
        for (TestPlugins plugin : plugins)
        {
            uninstallPluginAndVerify(plugin);
        }
    }

    /**
     * Note that this only works if the refimpl-spi-plugin is enabled, otherwise will get a 404.
     *
     * @param permission the {@code Permission} to be added
     * @return the {@code ClientResponse} of the resource call
     */
    public ClientResponse addPermission(Permission permission)
    {
        WebResource permissionResource = client.resource(URI.create(getStandardApplicationProperties().getBaseUrl() + "/rest/permissions/1.0/" + permission.name()));
        return permissionResource
            .type(APPLICATION_JSON)
            .accept(APPLICATION_JSON)
            .post(ClientResponse.class);
    }

    /**
     * Note that this only works if the refimpl-spi-plugin is enabled, otherwise will get a 404.
     *
     * @return the {@code ClientResponse} of the resource call
     */
    public ClientResponse addAllPermissions()
    {
        WebResource permissionResource = client.resource(URI.create(getStandardApplicationProperties().getBaseUrl() + "/rest/permissions/1.0/all"));
        return permissionResource
            .type(APPLICATION_JSON)
            .accept(APPLICATION_JSON)
            .post(ClientResponse.class);
    }

    /**
     * Note that this only works if the refimpl-spi-plugin is enabled, otherwise will get a 404.
     *
     * @param permission the {@code Permission} to be removed
     * @return the {@code ClientResponse} of the resource call
     */
    public ClientResponse removePermission(Permission permission)
    {
        WebResource permissionResource = client.resource(URI.create(getStandardApplicationProperties().getBaseUrl() + "/rest/permissions/1.0/" + permission.name()));
        return permissionResource
            .type(APPLICATION_JSON)
            .accept(APPLICATION_JSON)
            .delete(ClientResponse.class);
    }

    /**
     * Note that this only works if the refimpl-spi-plugin is enabled, otherwise will get a 404.
     *
     * @return the {@code ClientResponse} of the resource call
     */
    public ClientResponse removeAllPermissions()
    {
        WebResource permissionResource = client.resource(URI.create(getStandardApplicationProperties().getBaseUrl() + "/rest/permissions/1.0/all"));
        return permissionResource
            .type(APPLICATION_JSON)
            .accept(APPLICATION_JSON)
            .delete(ClientResponse.class);
    }

    /**
     * Note that this only works if the refimpl-spi-plugin is enabled, otherwise will get a 404.
     *
     * @return the {@code ClientResponse} of the resource call
     */
    public ClientResponse resetPermissions()
    {
        WebResource permissionResource = client.resource(URI.create(getStandardApplicationProperties().getBaseUrl() + "/rest/permissions/1.0/reset"));
        return permissionResource
            .type(APPLICATION_JSON)
            .accept(APPLICATION_JSON)
            .post(ClientResponse.class);
    }

    /**
     * Note that this only works if the refimpl-spi-plugin is enabled, otherwise will get a 404.
     *
     * @param plugin the plugin to have permissions denied
     * @return the {@code ClientResponse} of the resource call
     */
    public ClientResponse blacklistPluginPermissions(TestPlugins plugin)
    {
        WebResource permissionResource = client.resource(URI.create(getStandardApplicationProperties().getBaseUrl() + "/rest/permissions/1.0/plugin/" + plugin.getKey()));
        return permissionResource
            .type(APPLICATION_JSON)
            .accept(APPLICATION_JSON)
            .post(ClientResponse.class);
    }

    /**
     * Note that this only works if the refimpl-spi-plugin is enabled, otherwise will get a 404.
     *
     * @param plugin the plugin to have permissions behave as normal
     * @return the {@code ClientResponse} of the resource call
     */
    public ClientResponse unblacklistPluginPermissions(TestPlugins plugin)
    {
        WebResource permissionResource = client.resource(URI.create(getStandardApplicationProperties().getBaseUrl() + "/rest/permissions/1.0/plugin/" + plugin.getKey()));
        return permissionResource
            .type(APPLICATION_JSON)
            .accept(APPLICATION_JSON)
            .delete(ClientResponse.class);
    }

    /**
     * Note that this only works if the refimpl-spi-plugin is enabled, otherwise will get a 404.
     *
     * @param plugin the plugin the module belongs to
     * @param moduleKey the module key to have permissions denied
     * @return the {@code ClientResponse} of the resource call
     */
    public ClientResponse blacklistModulePermissions(TestPlugins plugin, String moduleKey)
    {
        WebResource permissionResource = client.resource(URI.create(getStandardApplicationProperties().getBaseUrl() + "/rest/permissions/1.0/module/" + plugin.getKey() + ":" + moduleKey));
        return permissionResource
            .type(APPLICATION_JSON)
            .accept(APPLICATION_JSON)
            .post(ClientResponse.class);
    }

    /**
     * Note that this only works if the refimpl-spi-plugin is enabled, otherwise will get a 404.
     *
     * @param plugin the plugin the module belongs to
     * @param moduleKey the module key to have permissions behave as normal
     * @return the {@code ClientResponse} of the resource call
     */
    public ClientResponse unblacklistModulePermissions(TestPlugins plugin, String moduleKey)
    {
        WebResource permissionResource = client.resource(URI.create(getStandardApplicationProperties().getBaseUrl() + "/rest/permissions/1.0/module/" + plugin.getKey() + ":" + moduleKey));
        return permissionResource
            .type(APPLICATION_JSON)
            .accept(APPLICATION_JSON)
            .delete(ClientResponse.class);
    }

    public CollectionRepresentation<BundleSummaryRepresentation> getBundles()
    {
        WebResource bundles = client.resource(uriBuilder.buildOsgiBundleCollectionUri());
        return bundles
            .accept(OSGI_BUNDLE_COLLECTION_JSON)
            .get(new GenericType<CollectionRepresentation<BundleSummaryRepresentation>>() {});
    }

    public BundleRepresentation getBundle(long id)
    {
        WebResource bundle = client.resource(uriBuilder.buildOsgiBundleUri(id));
        return bundle.accept(OSGI_BUNDLE_JSON).get(BundleRepresentation.class);
    }

    public CollectionRepresentation<ServiceSummaryRepresentation> getServices()
    {
        WebResource services = client.resource(uriBuilder.buildOsgiServiceCollectionUri());
        return services
            .accept(OSGI_SERVICE_COLLECTION_JSON)
            .get(new GenericType<CollectionRepresentation<ServiceSummaryRepresentation>>() {});
    }

    public CollectionRepresentation<PackageSummaryRepresentation> getPackages()
    {
        WebResource packages = client.resource(uriBuilder.buildOsgiPackageCollectionUri());
        return packages
            .accept(OSGI_PACKAGE_COLLECTION_JSON)
            .get(new GenericType<CollectionRepresentation<PackageSummaryRepresentation>>() {});
    }
    
    public URI getBaseUri()
    {
        return URI.create(applicationProperties.getBaseUrl());
    }

}
