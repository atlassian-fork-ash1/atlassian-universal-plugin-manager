package com.atlassian.upm.test;

import javax.ws.rs.core.Response.Status;

import com.sun.jersey.api.client.ClientResponse;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;

public final class JerseyClientMatchers
{
    private JerseyClientMatchers()
    {
    }

    public static Matcher<? super ClientResponse> ok()
    {
        return new StatusMatcher(Status.OK);
    }

    public static Matcher<? super ClientResponse> accepted()
    {
        return new StatusMatcher(Status.ACCEPTED);
    }

    public static Matcher<? super ClientResponse> badRequest()
    {
        return new StatusMatcher(Status.BAD_REQUEST);
    }

    public static Matcher<? super ClientResponse> notFound()
    {
        return new StatusMatcher(Status.NOT_FOUND);
    }

    public static Matcher<? super ClientResponse> conflict()
    {
        return new StatusMatcher(Status.CONFLICT);
    }

    public static Matcher<? super ClientResponse> badProxy()
    {
        return new StatusMatcher(502);
    }

    public static Matcher<? super ClientResponse> forbidden()
    {
        return new StatusMatcher(Status.FORBIDDEN);
    }

    public static Matcher<? super ClientResponse> unauthorized()
    {
        return new StatusMatcher(Status.UNAUTHORIZED);
    }

    private static final class StatusMatcher extends TypeSafeDiagnosingMatcher<ClientResponse>
    {
        private final int expected;

        public StatusMatcher(int status)
        {
            expected = status;
        }

        public StatusMatcher(Status status)
        {
            this(status.getStatusCode());
        }

        @Override
        protected boolean matchesSafely(ClientResponse item, Description mismatchDescription)
        {
            if (item.getStatus() != expected)
            {
                mismatchDescription.appendText("status was ").appendValue(item.getStatus());
                return false;
            }
            return true;
        }

        public void describeTo(Description description)
        {
            description.appendText("status is ").appendValue(expected);
        }
    }
}
