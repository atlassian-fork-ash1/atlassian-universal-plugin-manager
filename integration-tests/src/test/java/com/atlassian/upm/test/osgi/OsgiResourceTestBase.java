package com.atlassian.upm.test.osgi;

import com.atlassian.upm.osgi.rest.representations.BundleRepresentation;
import com.atlassian.upm.osgi.rest.representations.BundleSummaryRepresentation;
import com.atlassian.upm.osgi.rest.representations.CollectionRepresentation;
import com.atlassian.upm.osgi.rest.representations.PackageSummaryRepresentation;
import com.atlassian.upm.osgi.rest.representations.ServiceSummaryRepresentation;
import com.atlassian.upm.test.UpmResourceTestBase;

import com.google.common.base.Predicate;

import org.junit.Before;

import static com.google.common.collect.Iterables.find;

public class OsgiResourceTestBase extends UpmResourceTestBase
{
    protected static final String TEST_BUNDLE_SYMBOLIC_NAME = "test-plugin-v2-not-reloadable";

    protected CollectionRepresentation<BundleSummaryRepresentation> bundles;
    protected CollectionRepresentation<ServiceSummaryRepresentation> services;
    protected CollectionRepresentation<PackageSummaryRepresentation> packages;
    protected BundleRepresentation testBundle;

    @Before
    public void setUp()
    {
        bundles = restTester.getBundles();
        services = restTester.getServices();
        packages = restTester.getPackages();

        BundleSummaryRepresentation testBundleSummary = find(bundles.getEntries(), isTestBundle);
        testBundle = restTester.getBundle(testBundleSummary.getId());
    }

    private static final Predicate<BundleSummaryRepresentation> isTestBundle = new Predicate<BundleSummaryRepresentation>()
    {
        public boolean apply(BundleSummaryRepresentation bundleSummaryRepresentation)
        {
            return TEST_BUNDLE_SYMBOLIC_NAME.equals(bundleSummaryRepresentation.getSymbolicName());
        }
    };
}
