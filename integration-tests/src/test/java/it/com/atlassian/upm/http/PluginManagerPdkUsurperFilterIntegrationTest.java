package it.com.atlassian.upm.http;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import com.atlassian.upm.test.UpmUiTestBase;

import com.sun.jersey.api.client.ClientResponse;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.FilePartSource;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.junit.Test;

import static com.atlassian.integrationtesting.ApplicationPropertiesImpl.getStandardApplicationProperties;
import static com.atlassian.upm.test.JerseyClientMatchers.ok;
import static com.atlassian.upm.test.TestPlugins.INSTALLABLE;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;

public class PluginManagerPdkUsurperFilterIntegrationTest extends UpmUiTestBase
{
    private static final String TEST_PLUGIN_PATH = "test-jars/atlassian-universal-plugin-manager-test-plugin-v2-installable.jar";
    private static final File PLUGIN_FILE = new File(PluginManagerPdkUsurperFilterIntegrationTest.class.getClassLoader().getResource(TEST_PLUGIN_PATH).getFile());
    private static final File NON_PLUGIN_FILE = new File(PluginManagerPdkUsurperFilterIntegrationTest.class.getClassLoader().getResource("not-a-plugin.txt").getFile());

    @Test
    public void testThatAPluginIsInstalledViaPdk() throws Exception
    {
        try
        {
            final UploadResponse uploadResponse = uploadFile(PLUGIN_FILE, "admin", "admin");

            assertEquals(HttpStatus.SC_OK, uploadResponse.responseCode);

            assertThat(restTester.getPlugin(INSTALLABLE, ClientResponse.class), is(ok()));
        }
        finally
        {
            // Lets clean up after and uninstall the plugin via the rest tester
            restTester.uninstallPluginAndVerify(INSTALLABLE);
        }
    }

    @Test
    public void testThatAPluginWithFilePartSourceNameWithAbsolutePathIsInstalledViaPdk() throws Exception
    {
        try
        {
            final UploadResponse uploadResponse = uploadFile(PLUGIN_FILE, PLUGIN_FILE.getAbsolutePath(), "admin", "admin");

            assertEquals(HttpStatus.SC_OK, uploadResponse.responseCode);

            assertThat(restTester.getPlugin(INSTALLABLE, ClientResponse.class), is(ok()));
        }
        finally
        {
            // Lets clean up after and uninstall the plugin via the rest tester
            restTester.uninstallPluginAndVerify(INSTALLABLE);
        }
    }

    @Test
    public void testThatAPluginWithFilePartSourceNameWithBackslashJarPathIsInstalledViaPdk() throws Exception
    {
        try
        {
            final UploadResponse uploadResponse = uploadFile(PLUGIN_FILE, "\\.jar", "admin", "admin");

            assertEquals(HttpStatus.SC_OK, uploadResponse.responseCode);

            assertThat(restTester.getPlugin(INSTALLABLE, ClientResponse.class), is(ok()));
        }
        finally
        {
            // Lets clean up after and uninstall the plugin via the rest tester
            restTester.uninstallPluginAndVerify(INSTALLABLE);
        }
    }

    @Test
    public void testThatAPluginWithFilePartSourceNameWithSlashJarPathIsInstalledViaPdk() throws Exception
    {
        try
        {
            final UploadResponse uploadResponse = uploadFile(PLUGIN_FILE, "/.jar", "admin", "admin");

            assertEquals(HttpStatus.SC_OK, uploadResponse.responseCode);

            assertThat(restTester.getPlugin(INSTALLABLE, ClientResponse.class), is(ok()));
        }
        finally
        {
            // Lets clean up after and uninstall the plugin via the rest tester
            restTester.uninstallPluginAndVerify(INSTALLABLE);
        }
    }

    @Test
    public void testThatAPluginWithFilePartSourceNameWithWeirdJarPathIsInstalledViaPdk() throws Exception
    {
        try
        {
            final UploadResponse uploadResponse = uploadFile(PLUGIN_FILE, "<script>alert(1)<script>.jar", "admin", "admin");

            assertEquals(HttpStatus.SC_OK, uploadResponse.responseCode);

            assertThat(restTester.getPlugin(INSTALLABLE, ClientResponse.class), is(ok()));
        }
        finally
        {
            // Lets clean up after and uninstall the plugin via the rest tester
            restTester.uninstallPluginAndVerify(INSTALLABLE);
        }
    }

    @Test
    public void testThatANonPluginIsNotInstalledViaPdk() throws Exception
    {
        final UploadResponse uploadResponse = uploadFile(NON_PLUGIN_FILE, "admin", "admin");

        assertEquals(HttpStatus.SC_BAD_REQUEST, uploadResponse.responseCode);
        assertTrue(uploadResponse.responseBody.contains("not a valid plugin"));
    }

    @Test
    public void testThatAPluginIsNotInstalledWhenUserIsInvalidViaPdk() throws Exception
    {
        final UploadResponse uploadResponse = uploadFile(PLUGIN_FILE, "dude", "dude");

        assertEquals(uploadResponse.responseCode, HttpStatus.SC_UNAUTHORIZED);
    }

    @Test
    public void testThatAPluginIsNotInstalledViaPdkWhenInSafeMode() throws Exception
    {
        try
        {
            // Stick the system into safemode
            restTester.enterSafeMode();

            final UploadResponse uploadResponse = uploadFile(PLUGIN_FILE, "admin", "admin");

            assertEquals(HttpStatus.SC_CONFLICT, uploadResponse.responseCode);
            assertTrue(uploadResponse.responseBody.contains("Cannot install a plugin when system is in Safe Mode."));
        }
        finally
        {
            restTester.exitSafeModeAndRestoreSavedConfiguration();
        }
    }

    private UploadResponse uploadFile(final File pluginFile, final String username, final String password) throws Exception
    {
        return uploadFile(pluginFile, pluginFile.getName(), username, password);
    }

    private UploadResponse uploadFile(final File pluginFile, final String filePartSourceName, final String username, final String password) throws Exception
    {
        // Create an instance of HttpClient.
        HttpClient client = new HttpClient();
        client.getParams().setAuthenticationPreemptive(true);
        client.getHttpConnectionManager().getParams().setConnectionTimeout(5000);
        Credentials defaultcreds = new UsernamePasswordCredentials(username, password);
        client.getState().setCredentials(AuthScope.ANY, defaultcreds);

        // Create a method instance.
        String url = getStandardApplicationProperties().getBaseUrl() + "/admin/uploadplugin.action";

        PostMethod filePost = new PostMethod(url);
        Part[] parts = new Part[]{new FilePart("file_0", new FilePartSource(filePartSourceName, pluginFile))};
        filePost.setRequestEntity(new MultipartRequestEntity(parts, filePost.getParams()));
        filePost.setFollowRedirects(false);

        // bypass anti xsrf and websudo protection
        filePost.setRequestHeader("X-Atlassian-Token", "no-check");
        filePost.setRequestHeader("X-Atlassian-WebSudo", "no-check");

        try
        {
            // Execute the method.
            final int status = client.executeMethod(filePost);
            return new UploadResponse(status, filePost.getResponseBodyAsString());

        }
        catch (Exception e)
        {
            fail("Unable to execute the post.");
            return null;
        }
        finally
        {
            filePost.releaseConnection();
        }
    }

    protected String urlEncode(String text)
    {
        try
        {
            return URLEncoder.encode(text, "UTF-8");
        }
        catch (UnsupportedEncodingException e)
        {
            return text;
        }
    }

    private class UploadResponse
    {
        final int responseCode;
        final String responseBody;

        private UploadResponse(final int responseCode, final String responseBody)
        {
            this.responseCode = responseCode;
            this.responseBody = responseBody;
        }
    }

}
