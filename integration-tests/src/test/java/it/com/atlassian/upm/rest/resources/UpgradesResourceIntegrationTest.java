package it.com.atlassian.upm.rest.resources;

import com.atlassian.upm.rest.representations.UpgradesRepresentation;
import com.atlassian.upm.test.UpmResourceTestBase;

import org.junit.Test;

import static com.atlassian.upm.test.TestPlugins.BROKEN_UPGRADE_V1;
import static com.atlassian.upm.test.TestPlugins.BUNDLED;
import static com.atlassian.upm.test.TestPlugins.CANNOT_DISABLE_MODULE;
import static com.atlassian.upm.test.TestPlugins.INSTALLABLE;
import static com.atlassian.upm.test.TestPlugins.LEGACY_PLUGIN;
import static com.atlassian.upm.test.TestPlugins.UPM;
import static com.atlassian.upm.test.UpmIntegrationMatchers.containsPlugins;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class UpgradesResourceIntegrationTest extends UpmResourceTestBase
{
    @Test
    public void assertThatUpgradesResourceReturnsAvailableUpgrades()
    {
        UpgradesRepresentation upgrades = restTester.getUpgrades();
        assertThat(upgrades.getPlugins(), containsPlugins(UPM, INSTALLABLE, BUNDLED, LEGACY_PLUGIN, CANNOT_DISABLE_MODULE, BROKEN_UPGRADE_V1));
    }

    @Test
    public void assertThatUpgradesPluginsHasSafeModeFalseWhenNotInSafeMode()
    {
        UpgradesRepresentation upgrades = restTester.getUpgrades();
        assertFalse(upgrades.isSafeMode());
    }

    @Test
    public void assertThatUpgradesPluginsHasSafeModeTrueWhenInSafeMode()
    {
        restTester.enterSafeMode();
        try
        {
            UpgradesRepresentation upgrades = restTester.getUpgrades();
            assertTrue(upgrades.isSafeMode());
        }
        finally
        {
            restTester.exitSafeModeAndRestoreSavedConfiguration();
        }
    }
}
