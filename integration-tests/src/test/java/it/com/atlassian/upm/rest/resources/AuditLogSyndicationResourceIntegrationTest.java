package it.com.atlassian.upm.rest.resources;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import com.atlassian.integrationtesting.ApplicationPropertiesImpl;
import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.upm.test.AsynchronousTaskException;
import com.atlassian.upm.test.TestPlugins;
import com.atlassian.upm.test.UpmResourceTestBase;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.syndication.feed.atom.Entry;
import com.sun.syndication.feed.atom.Feed;
import com.sun.syndication.feed.atom.Person;

import org.codehaus.jettison.json.JSONException;
import org.jdom.Element;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.upm.test.TestPlugins.BUNDLED;
import static com.atlassian.upm.test.TestPlugins.INSTALLABLE;
import static com.atlassian.upm.test.UpmIntegrationMatchers.feedWithEntriesContaining;
import static com.atlassian.upm.test.UpmIntegrationMatchers.feedWithLinkRelAndStartIndex;
import static com.atlassian.upm.test.UpmIntegrationMatchers.feedWithNonexistantLinkRel;
import static com.atlassian.upm.test.UpmTestGroups.CONFLUENCE;
import static com.atlassian.upm.test.UpmTestGroups.FECRU;
import static com.atlassian.upm.test.UpmTestGroups.JIRA;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.OK;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class AuditLogSyndicationResourceIntegrationTest extends UpmResourceTestBase
{
    private static final String TEST_USERNAME = "admin";
    private static final String TEST_USERFULLNAME = "A. D. Ministrator (Sysadmin)";
    private static final String TEST_FECRU_USERFULLNAME = "A. D. Ministrator";
    public static final int TEST_PURGE_AFTER = 30;
    public static final int TEST_MAX_ENTRIES = 5000;

    @Before
    public void setUp()
    {
        restTester.purgeAuditLogFeed();
        restTester.setAuditLogMaxEntries(TEST_MAX_ENTRIES);
        restTester.setAuditLogPurgeAfter(TEST_PURGE_AFTER);
    }

    @After
    public void tearDown()
    {
        restTester.purgeAuditLogFeed();
        restTester.setAuditLogMaxEntries(TEST_MAX_ENTRIES);
        restTester.setAuditLogPurgeAfter(TEST_PURGE_AFTER);
    }

    @Test
    public void disablePluginAddsEntryToFeed() throws URISyntaxException
    {
        try
        {
            ClientResponse response = restTester.disablePlugin(BUNDLED);
            assertThat(response.getResponseStatus(), is(OK));
            Feed feed = restTester.getAuditLogFeed();
            assertThat(feed.getEntries().size(), is(equalTo(1)));
        }
        finally
        {
            // re-enable plugin, other tests expects it to be enabled
            restTester.enablePlugin(BUNDLED);
        }
    }

    @Test
    public void unsuccessfulInstallAddsEntryToFeed() throws JSONException
    {
        URI pluginKey = URI.create(ApplicationPropertiesImpl.getStandardApplicationProperties().getBaseUrl() + "/i-do-not-exist").normalize();
        try
        {
            restTester.installPluginAndWaitForCompletion(pluginKey);
        }
        catch (AsynchronousTaskException e)
        {
            Feed feed = restTester.getAuditLogFeed();
            assertThat(feed.getEntries().size(), is(equalTo(1)));
            assertThat(((Entry) feed.getEntries().get(0)).getTitle(), is(equalTo("Failed to install plugin " + pluginKey)));
        }
    }

    @Test
    public void enteringSafeModeAddsStartingMessage() throws JSONException
    {
        try
        {
            restTester.enterSafeMode();
            Feed feed = restTester.getAuditLogFeed();
            List<Entry> entries = getEntries(feed);
            assertThat(entries.get(entries.size() - 1).getTitle(), is(equalTo("System entering Safe Mode...")));
        }
        finally
        {
            restTester.exitSafeModeAndRestoreSavedConfiguration();
        }
    }

    @Test
    public void enteringSafeModeAddsSuccessfulMessage() throws JSONException
    {
        try
        {
            restTester.enterSafeMode();
            Feed feed = restTester.getAuditLogFeed();
            assertThat(getEntries(feed).get(0).getTitle(), is(equalTo("System successfully entered Safe Mode")));
        }
        finally
        {
            restTester.exitSafeModeAndRestoreSavedConfiguration();
        }
    }

    @Test
    public void exitingSafeModeAddsStartingMessage() throws JSONException
    {
        try
        {
            restTester.enterSafeMode();
            restTester.purgeAuditLogFeed();

            restTester.exitSafeModeAndRestoreSavedConfiguration();
            Feed feed = restTester.getAuditLogFeed();
            List<Entry> entries = getEntries(feed);
            assertThat(entries.get(entries.size() - 1).getTitle(), is(equalTo("System exiting Safe Mode...")));
        }
        finally
        {
            //in case anything bad happened during the test run
            restTester.exitSafeModeAndRestoreSavedConfiguration();
        }
    }

    @Test
    public void exitingSafeModeAddsSuccessfulMessage() throws JSONException
    {
        try
        {
            restTester.enterSafeMode();
            restTester.purgeAuditLogFeed();

            restTester.exitSafeModeAndRestoreSavedConfiguration();
            Feed feed = restTester.getAuditLogFeed();
            assertThat(getEntries(feed).get(0).getTitle(), is(equalTo("System successfully exited Safe Mode")));
        }
        finally
        {
            //in case anything bad happened during the test run
            restTester.exitSafeModeAndRestoreSavedConfiguration();
        }
    }

    @Test
    public void feedPagingMaxResults() throws URISyntaxException
    {
        try
        {
            restTester.disablePlugin(BUNDLED);
            restTester.enablePlugin(BUNDLED);
            Feed feedSizeOf1 = restTester.getAuditLogFeed(0, 1);
            assertThat(feedSizeOf1.getEntries().size(), is(equalTo(1)));
        }
        finally
        {
            // re-enable plugin, other tests expects it to be enabled
            restTester.enablePlugin(BUNDLED);
        }
    }

    @Test
    public void feedPagingStartIndex() throws URISyntaxException
    {
        try
        {
            restTester.disablePlugin(BUNDLED);
            restTester.enablePlugin(BUNDLED);

            Feed feedStart0 = restTester.getAuditLogFeed(0, 2);
            Feed feedStart1 = restTester.getAuditLogFeed(1, 2);
            assertThat(feedStart0.getEntries().get(1), is(equalTo(feedStart1.getEntries().get(0))));
        }
        finally
        {
            // re-enable plugin, other tests expects it to be enabled
            restTester.enablePlugin(BUNDLED);
        }
    }

    @Test
    public void workerThreadLoggingCorrectUsername() throws JSONException
    {
        try
        {
            restTester.installPluginAndWaitForCompletion(INSTALLABLE);

            Feed feed = restTester.getAuditLogFeed();
            for (Entry entry : getEntries(feed))
            {
                final List<Person> authors = getAuthors(entry);
                assertThat(authors.size(), is(1));
                String userFullName;

                // UPM-962 - the users in feCru AMPS data are not what we expect
                final String testGroup = System.getProperty("testGroup", "refapp");
                if (testGroup.equalsIgnoreCase(FECRU))
                {
                    userFullName = TEST_FECRU_USERFULLNAME;
                }
                else if (testGroup.equalsIgnoreCase(CONFLUENCE) || testGroup.equalsIgnoreCase(JIRA))
                {
                    // The conf data admin users fullname has not been changed, this is a work-around to avoid publishing new data.
                    userFullName = TEST_USERNAME;
                }
                else
                {
                    userFullName = TEST_USERFULLNAME;
                }
                assertThat(authors.get(0).getName(), is(userFullName));
            }
        }
        finally
        {
            //re-uninstall it, other tests expect it to be uninstalled
            restTester.uninstallPluginAndVerify(INSTALLABLE.getKey());
        }
    }

    @Test
    public void purgeAfterWithValidValue()
    {
        try
        {
            ClientResponse response = restTester.setAuditLogPurgeAfter(TEST_PURGE_AFTER + 1);
            assertThat(response.getResponseStatus(), is(OK));
            assertThat(restTester.getAuditLogPurgeAfter(), is(equalTo(TEST_PURGE_AFTER + 1)));
        }
        finally
        {
            restTester.setAuditLogPurgeAfter(TEST_PURGE_AFTER);
        }
    }

    @Test
    public void purgeAfterWithNegativeValueIsIgnored()
    {
        ClientResponse response = restTester.setAuditLogPurgeAfter(-1);
        assertThat(response.getResponseStatus(), is(BAD_REQUEST));
        assertThat(restTester.getAuditLogPurgeAfter(), is(equalTo(TEST_PURGE_AFTER)));
    }

    @Test
    public void purgeAfterWithZeroValueReturnsBadRequest()
    {
        ClientResponse response = restTester.setAuditLogPurgeAfter(0);
        assertThat(response.getResponseStatus(), is(BAD_REQUEST));
        assertThat(restTester.getAuditLogPurgeAfter(), is(equalTo(TEST_PURGE_AFTER)));
    }

    @Test
    public void purgeAfterWithOverlyLargeValueReturnsBadRequest()
    {
        ClientResponse response = restTester.setAuditLogPurgeAfter(100001);
        assertThat(response.getResponseStatus(), is(BAD_REQUEST));
        assertThat(restTester.getAuditLogPurgeAfter(), is(equalTo(TEST_PURGE_AFTER)));
    }

    @Test
    public void maxEntriesWithValidValue()
    {
        try
        {
            ClientResponse response = restTester.setAuditLogMaxEntries(TEST_MAX_ENTRIES + 1);
            assertThat(response.getResponseStatus(), is(OK));
            assertThat(restTester.getAuditLogMaxEntries(), is(equalTo(TEST_MAX_ENTRIES + 1)));
        }
        finally
        {
            restTester.setAuditLogMaxEntries(TEST_MAX_ENTRIES);
        }
    }

    @Test
    public void maxEntriesWithNegativeValueIsIgnored()
    {
        ClientResponse response = restTester.setAuditLogMaxEntries(-1);
        assertThat(response.getResponseStatus(), is(BAD_REQUEST));
        assertThat(restTester.getAuditLogMaxEntries(), is(equalTo(TEST_MAX_ENTRIES)));
    }

    @Test
    public void exceedingMaxEntriesStoresLatestEntries() throws URISyntaxException
    {
        try
        {
            restTester.setAuditLogMaxEntries(1);
            restTester.disablePlugin(BUNDLED);
            restTester.enablePlugin(BUNDLED);

            Feed feed = restTester.getAuditLogFeed();
            assertThat(feed, is(feedWithEntriesContaining(BUNDLED)));
        }
        finally
        {
            // re-enable plugin, other tests expects it to be enabled
            restTester.enablePlugin(BUNDLED);
        }

    }

    @Test
    public void verifyNextPagingLinkIsCorrect() throws URISyntaxException
    {
        try
        {
            restTester.disablePlugin(BUNDLED);
            restTester.enablePlugin(BUNDLED);

            Feed feed = restTester.getAuditLogFeed(0, 1);
            assertThat(feed, is(feedWithLinkRelAndStartIndex("next", 1)));
        }
        finally
        {
            //in case something weird goes on, make sure it is enabled when the test is complete
            restTester.enablePlugin(BUNDLED);
        }
    }

    @Test
    public void verifyPreviousPagingLinkIsCorrect() throws URISyntaxException
    {
        try
        {
            restTester.disablePlugin(BUNDLED);
            restTester.enablePlugin(BUNDLED);

            Feed feed = restTester.getAuditLogFeed(1, 1);
            assertThat(feed, is(feedWithLinkRelAndStartIndex("previous", 0)));
        }
        finally
        {
            //in case something weird goes on, make sure it is enabled when the test is complete
            restTester.enablePlugin(BUNDLED);
        }
    }

    @Test
    public void verifyFirstPagingLinkIsCorrect() throws URISyntaxException
    {
        try
        {
            restTester.disablePlugin(BUNDLED);
            restTester.enablePlugin(BUNDLED);

            Feed feed = restTester.getAuditLogFeed(1, 1);
            assertThat(feed, is(feedWithLinkRelAndStartIndex("first", 0)));
        }
        finally
        {
            //in case something weird goes on, make sure it is enabled when the test is complete
            restTester.enablePlugin(BUNDLED);
        }
    }

    @Test
    public void verifyLastPagingLinkIsCorrect() throws URISyntaxException
    {
        try
        {
            restTester.disablePlugin(BUNDLED);
            restTester.enablePlugin(BUNDLED);

            Feed feed = restTester.getAuditLogFeed(0, 1);
            assertThat(feed, is(feedWithLinkRelAndStartIndex("last", 1)));
        }
        finally
        {
            //in case something weird goes on, make sure it is enabled when the test is complete
            restTester.enablePlugin(BUNDLED);
        }
    }

    @Test
    public void verifyThatTotalEntriesForeignMarkupContainsTotalNumberOfEntries() throws URISyntaxException
    {
        try
        {
            restTester.disablePlugin(BUNDLED);
            restTester.enablePlugin(BUNDLED);

            Feed feed = restTester.getAuditLogFeed();
            assertThat(getTotalEntries(feed), is(equalTo(2)));
        }
        finally
        {
            restTester.enablePlugin(BUNDLED);
        }
    }

    @Test
    public void verifyThatStartIndexForeignMarkupContainsStartIndex() throws URISyntaxException
    {
        try
        {
            restTester.disablePlugin(BUNDLED);
            restTester.enablePlugin(BUNDLED);

            Feed feed = restTester.getAuditLogFeed(1, 1);
            assertThat(getStartIndex(feed), is(equalTo(1)));
        }
        finally
        {
            restTester.enablePlugin(BUNDLED);
        }
    }

    @Test
    public void verifyNextPagingLinkDoesNotExistWhenOnLastPage() throws URISyntaxException
    {
        try
        {
            restTester.disablePlugin(BUNDLED);
            restTester.enablePlugin(BUNDLED);

            Feed feed = restTester.getAuditLogFeed(1, 1);
            assertThat(feed, is(feedWithNonexistantLinkRel("next")));
        }
        finally
        {
            //in case something weird goes on, make sure it is enabled when the test is complete
            restTester.enablePlugin(BUNDLED);
        }
    }

    @Test
    public void verifyPreviousPagingLinkDoesNotExistWhenOnFirstPage() throws URISyntaxException
    {
        try
        {
            restTester.disablePlugin(BUNDLED);
            restTester.enablePlugin(BUNDLED);

            Feed feed = restTester.getAuditLogFeed(0, 1);
            assertThat(feed, is(feedWithNonexistantLinkRel("previous")));
        }
        finally
        {
            //in case something weird goes on, make sure it is enabled when the test is complete
            restTester.enablePlugin(BUNDLED);
        }
    }

    @Test
    public void verifyFirstPagingLinkDoesNotExistWhenOnFirstPage() throws URISyntaxException
    {
        try
        {
            restTester.disablePlugin(BUNDLED);
            restTester.enablePlugin(BUNDLED);

            Feed feed = restTester.getAuditLogFeed(0, 1);
            assertThat(feed, is(feedWithNonexistantLinkRel("first")));
        }
        finally
        {
            //in case something weird goes on, make sure it is enabled when the test is complete
            restTester.enablePlugin(BUNDLED);
        }
    }

    @Test
    public void verifyLastPagingLinkDoesNotExistWhenOnLastPage() throws URISyntaxException
    {
        try
        {
            restTester.disablePlugin(BUNDLED);
            restTester.enablePlugin(BUNDLED);

            Feed feed = restTester.getAuditLogFeed(1, 1);
            assertThat(feed, is(feedWithNonexistantLinkRel("last")));
        }
        finally
        {
            //in case something weird goes on, make sure it is enabled when the test is complete
            restTester.enablePlugin(BUNDLED);
        }
    }

    @Test
    @TestGroups(excludes = CONFLUENCE, reason = "Confluence has better legacy plugin support than other products and thus will have 1 more success")
    public void upgradeAllAddsSuccessAndFailureMessageEntriesToFeedWithoutLegacySupport() throws Exception
    {
        try
        {
            upgradeAll();
            Feed feed = restTester.getAuditLogFeed();
            assertThat(feed.getEntries().size(), is(equalTo(5)));

            int upgradeSuccess = 0, upgradeFailure = 0;
            for (Entry entry : getEntries(feed))
            {
                if (entry.getTitle().startsWith("Installed plugin"))
                {
                    upgradeSuccess++;
                }
                else if (entry.getTitle().startsWith("Failed to upgrade plugin"))
                {
                    upgradeFailure++;
                }
            }
            assertThat(upgradeSuccess, is(equalTo(2)));
            assertThat(upgradeFailure, is(equalTo(3)));
        }
        finally
        {
            restTester.uninstallPluginAndVerify(TestPlugins.INSTALLABLE);
            restTester.uninstallPluginAndVerify(TestPlugins.CANNOT_DISABLE_MODULE);
        }
    }

    /**
     * v1 plugins can be upgraded in confluence, so the success/failure numbers differ here
     */
    @Test
    @TestGroups(value = CONFLUENCE, reason = "Confluence has better legacy plugin support than other products and thus will have 1 more success")
    public void upgradeAllAddsSuccessAndFailureMessageEntriesToFeedWithLegacySupport() throws Exception
    {
        try
        {
            upgradeAll();
            Feed feed = restTester.getAuditLogFeed();
            assertThat(feed.getEntries().size(), is(equalTo(5)));

            int upgradeSuccess = 0, upgradeFailure = 0;
            for (Entry entry : getEntries(feed))
            {
                if (entry.getTitle().startsWith("Installed plugin"))
                {
                    upgradeSuccess++;
                }
                else if (entry.getTitle().startsWith("Failed to upgrade plugin"))
                {
                    upgradeFailure++;
                }
            }
            assertThat(upgradeSuccess, is(equalTo(3)));
            assertThat(upgradeFailure, is(equalTo(2)));
        }
        finally
        {
            restTester.uninstallPluginAndVerify(TestPlugins.INSTALLABLE);
            restTester.uninstallPluginAndVerify(TestPlugins.LEGACY_PLUGIN);
            restTester.uninstallPluginAndVerify(TestPlugins.CANNOT_DISABLE_MODULE);
        }
    }

    private void upgradeAll() throws Exception
    {
        restTester.upgradeAllAndWaitForCompletion();
        //wait a bit in case the audit log messages haven't yet added in the asynchronous upgrade all task
        Thread.sleep(1000);
    }

    @SuppressWarnings("unchecked")
    private List<Person> getAuthors(final Entry entry)
    {
        return (List<Person>) entry.getAuthors();
    }

    @SuppressWarnings("unchecked")
    private List<Entry> getEntries(final Feed feed)
    {
        return (List<Entry>) feed.getEntries();
    }

    @SuppressWarnings("unchecked")
    private int getTotalEntries(final Feed feed)
    {
        for (Element e : (List<Element>) feed.getForeignMarkup())
        {
            if ("totalEntries".equals(e.getName()))
            {
                return Integer.parseInt(e.getText());
            }
        }
        throw new RuntimeException("total entries should exist but does not");
    }

    @SuppressWarnings("unchecked")
    private int getStartIndex(final Feed feed)
    {
        for (Element e : (List<Element>) feed.getForeignMarkup())
        {
            if ("startIndex".equals(e.getName()))
            {
                return Integer.parseInt(e.getText());
            }
        }
        throw new RuntimeException("start index should exist but does not");
    }
}
