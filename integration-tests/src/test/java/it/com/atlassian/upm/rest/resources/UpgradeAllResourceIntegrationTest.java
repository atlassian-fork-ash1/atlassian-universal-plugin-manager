package it.com.atlassian.upm.rest.resources;

import javax.ws.rs.core.MediaType;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.upm.rest.async.AsynchronousTask.Representation;
import com.atlassian.upm.rest.resources.upgradeall.UpgradeAllResults;
import com.atlassian.upm.rest.resources.upgradeall.UpgradeFailed;
import com.atlassian.upm.rest.resources.upgradeall.UpgradeStatus;
import com.atlassian.upm.rest.resources.upgradeall.UpgradeSucceeded;
import com.atlassian.upm.test.TestPlugins;
import com.atlassian.upm.test.UpmIntegrationMatchers.UpgradeFailedMatcher;
import com.atlassian.upm.test.UpmIntegrationMatchers.UpgradeSucceededMatcher;
import com.atlassian.upm.test.UpmResourceTestBase;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.junit.Test;

import static com.atlassian.upm.test.TestPlugins.CANNOT_DISABLE_MODULE;
import static com.atlassian.upm.test.UpmIntegrationMatchers.downloadFailedFor;
import static com.atlassian.upm.test.UpmIntegrationMatchers.upgradeFailureOf;
import static com.atlassian.upm.test.UpmIntegrationMatchers.upgradeSucceededFor;
import static com.atlassian.upm.test.UpmTestGroups.CONFLUENCE;
import static javax.ws.rs.core.Response.Status.CONFLICT;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertTrue;

public class UpgradeAllResourceIntegrationTest extends UpmResourceTestBase
{
    @Test
    public void assertThatTryingToUpgradeAllReturnsConflictWhenInSafeMode()
    {
        restTester.enterSafeMode();
        try
        {
            assertThat(restTester.upgradeAll().getStatus(), is(equalTo(CONFLICT.getStatusCode())));
        }
        finally
        {
            restTester.exitSafeModeAndRestoreSavedConfiguration();
        }
    }

    /**
     * Confluence supports dynamically installing legacy plugins, so should be able to upgrade them.
     */
    @Test
    @TestGroups(CONFLUENCE)
    public void assertThatUpgradingAllReturnsUpgradeResultsWithLegacyPluginUpgradedSuccessfully() throws Exception
    {
        assertThatUpgradingAllReturnsUpgradeResults(
            hasSuccesses(
                upgradeSucceededFor("Installable Test Plugin", "1.1"),
                upgradeSucceededFor("Test Plugin v1 (installable)", "1.2"),
                upgradeSucceededFor(CANNOT_DISABLE_MODULE.getName(), "1.2")
            ), hasFailures(
            downloadFailedFor("Non-downloadable Test Plugin Upgrade for bundled", "1.1"),
            upgradeFailureOf("Uninstallable Test Plugin Upgrade", "1.1")
        )
        );
    }

    /**
     * Products other than Confluence don't support legacy dynamically installing plugins (though you can statically
     * install them in Bamboo), so should report attempts to upgrade them as errors.
     */
    @Test
    @TestGroups(excludes = CONFLUENCE)
    public void assertThatUpgradingAllReturnsUpgradeResultsWithLegacyPluginUpgradeFailure() throws Exception
    {
        assertThatUpgradingAllReturnsUpgradeResults(
            hasSuccesses(
                upgradeSucceededFor("Installable Test Plugin", "1.1"),
                upgradeSucceededFor(CANNOT_DISABLE_MODULE.getName(), "1.2")
            ), hasFailures(
            downloadFailedFor("Non-downloadable Test Plugin Upgrade for bundled", "1.1"),
            upgradeFailureOf("Test Plugin v1 (installable)", "1.2"),
            upgradeFailureOf("Uninstallable Test Plugin Upgrade", "1.1")
        )
        );
    }

    private void assertThatUpgradingAllReturnsUpgradeResults(Matcher<UpgradeAllResults> successes, Matcher<UpgradeAllResults> failures) throws Exception
    {
        try
        {
            restTester.installPluginAndWaitForCompletion(TestPlugins.INSTALLABLE);
            restTester.installPluginAndWaitForCompletion(TestPlugins.BROKEN_UPGRADE_V1);
            restTester.uninstallPluginAndVerify(TestPlugins.CANNOT_DISABLE_MODULE);

            ClientResponse response = restTester.upgradeAllAndWaitForCompletion();
            assertTrue(response.getType().isCompatible(MediaType.valueOf(UpgradeStatus.State.COMPLETE.getContentType())));

            Representation<UpgradeAllResults> results = response.getEntity(new GenericType<Representation<UpgradeAllResults>>()
            {
            });
            assertThat(results.getStatus(), allOf(successes, failures));
        }
        finally
        {
            restTester.uninstallPluginAndVerify(TestPlugins.INSTALLABLE);
            restTester.uninstallPluginAndVerify(TestPlugins.BROKEN_UPGRADE_V1);
            restTester.uninstallPluginAndVerify(TestPlugins.CANNOT_DISABLE_MODULE);
        }
    }

    private Matcher<UpgradeAllResults> hasSuccesses(final UpgradeSucceededMatcher... successes)
    {
        final Matcher<Iterable<UpgradeSucceeded>> matcher = contains(successes);
        return new TypeSafeDiagnosingMatcher<UpgradeAllResults>()
        {
            @Override
            protected boolean matchesSafely(UpgradeAllResults item, Description mismatchDescription)
            {
                if (matcher.matches(item.getSuccesses()))
                {
                    return true;
                }
                matcher.describeMismatch(item.getSuccesses(), mismatchDescription);
                return false;
            }

            public void describeTo(Description description)
            {
                description.appendDescriptionOf(matcher);
            }
        };
    }

    private Matcher<UpgradeAllResults> hasFailures(final UpgradeFailedMatcher... failures)
    {
        final Matcher<Iterable<UpgradeFailed>> matcher = contains(failures);
        return new TypeSafeDiagnosingMatcher<UpgradeAllResults>()
        {
            @Override
            protected boolean matchesSafely(UpgradeAllResults item, Description mismatchDescription)
            {
                if (matcher.matches(item.getFailures()))
                {
                    return true;
                }
                matcher.describeMismatch(item.getFailures(), mismatchDescription);
                return false;
            }

            public void describeTo(Description description)
            {
                description.appendDescriptionOf(matcher);
            }
        };
    }
}
