package it.com.atlassian.upm.uitests;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URI;

import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.pageobjects.page.HomePage;
import com.atlassian.pageobjects.page.LoginPage;
import com.atlassian.pageobjects.page.WebSudoPage;
import com.atlassian.upm.pageobjects.ConfirmDialog;
import com.atlassian.upm.pageobjects.InstalledPlugin;
import com.atlassian.upm.pageobjects.InstalledPluginDetails;
import com.atlassian.upm.pageobjects.ManageExistingTab;
import com.atlassian.upm.pageobjects.PluginManager;
import com.atlassian.upm.test.TestPlugins;
import com.atlassian.upm.test.UpmUiTestRunner;
import com.atlassian.webdriver.pageobjects.WebDriverTester;

import com.google.inject.Inject;

import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.upm.pageobjects.ManageExistingTab.Category.USER_SECTION;
import static com.atlassian.upm.pageobjects.ManageExistingTab.Category.SYSTEM_SECTION;
import static com.atlassian.upm.test.TestPlugins.BUNDLED;
import static com.atlassian.upm.test.TestPlugins.BUNDLED_WITH_CONFIG;
import static com.atlassian.upm.test.TestPlugins.CANNOT_DISABLE_MODULE;
import static com.atlassian.upm.test.TestPlugins.INSTALLABLE;
import static com.atlassian.upm.test.TestPlugins.INSTALLABLE_REQUIRES_RESTART;
import static com.atlassian.upm.test.TestPlugins.SYSTEM;
import static com.atlassian.upm.test.TestPlugins.UPGRADEABLE_REQUIRES_RESTART_V2;
import static com.atlassian.upm.test.TestPlugins.UPM;
import static com.atlassian.upm.test.TestPlugins.USER_INSTALLED_WITH_MODULES;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(UpmUiTestRunner.class)
public class ManageExistingTabTest
{
    private static @Inject TestedProduct<WebDriverTester> product;

    private static PluginManager upm;
    private static ManageExistingTab tab;

    @BeforeClass
    public static void setUp()
    {
        upm = product.visit(LoginPage.class).
            loginAsSysAdmin(WebSudoPage.class).
            confirm(PluginManager.class);
        installPlugin(INSTALLABLE);
    }

    @AfterClass
    public static void uninstallPlugin()
    {
        tab.uninstallPlugin(INSTALLABLE.getKey());

        product.visit(HomePage.class).getHeader().logout(LoginPage.class);
    }

    @Before
    public void loadTab()
    {
        reloadTab();
    }

    @Test
    public void userInstalledPluginsGroupContainsUserInstalledPlugin() throws Exception
    {
        assertTrue(tab.getUserInstalledPlugins().contains(INSTALLABLE.getKey()));
    }

    @Test
    public void userInstalledPluginsGroupDoesNotContainsBundledPlugin()
    {
        assertFalse(tab.getUserInstalledPlugins().contains(BUNDLED.getKey()));
    }

    @Test
    public void userInstalledPluginsGroupDoesNotContainsSystemPlugin()
    {
        assertFalse(tab.getUserInstalledPlugins().contains(SYSTEM.getKey()));
    }

    @Test
    public void systemPluginsGroupContainsSystemPlugin()
    {
        assertTrue(tab.showSystemPlugins().contains(SYSTEM.getKey()));
    }

    @Test
    public void systemPluginsGroupContainsBundledPlugin()
    {
        assertTrue(tab.showSystemPlugins().contains(BUNDLED.getKey()));
    }

    @Test
    public void systemPluginsGroupDoesNotContainsUserInstalledPlugin() throws Exception
    {
        assertFalse(tab.showSystemPlugins().contains(INSTALLABLE.getKey()));
    }

    @Test
    public void userInstalledPluginsHaveDisableButton() throws Exception
    {
        assertTrue(tab.getPlugin(INSTALLABLE.getKey()).
            openPluginDetails().
            canBeDisabled());
    }

    @Test
    public void userInstalledPluginsHaveUninstallButton() throws Exception
    {
        assertTrue(tab.getPlugin(INSTALLABLE.getKey()).
            openPluginDetails().
            canBeUninstalled());
    }

    @Test
    public void userInstalledPluginsHavePluginDetailsLinkIfAvailable() throws Exception
    {
        assertTrue(tab.getPlugin(INSTALLABLE.getKey()).
            openPluginDetails().
            hasPluginDetailsLink());
    }

    @Test
    public void userInstalledPluginsHaveUninstallButtonEvenWhenPacBaseUrlDoesNotExist() throws Exception
    {
        withPacBaseUri(URI.create("http://does.not.compute"), new Runnable()
        {
            public void run()
            {
                assertTrue(tab.getPlugin(INSTALLABLE.getKey()).
                        openPluginDetails().
                        canBeUninstalled());
            }
        });
    }

    @Test
    public void systemPluginsDoNotHaveUninstallButton() throws IOException
    {
        assertFalse(tab.showSystemPlugins().
            getPlugin(BUNDLED.getKey()).
            openPluginDetails().
            canBeUninstalled());
    }

    @Test
    public void systemPluginsHavePluginDetailsLinkIfAvailable() throws IOException
    {
        assertTrue(tab.showSystemPlugins().
            getPlugin(BUNDLED.getKey()).
            openPluginDetails().
            hasPluginDetailsLink());
    }

    @Test
    public void systemPluginsDoNotHavePluginDetailsLinkIfNotAvailable() throws IOException
    {
        assertFalse(tab.showSystemPlugins().
            getPlugin(BUNDLED_WITH_CONFIG.getKey()).
            openPluginDetails().
            hasPluginDetailsLink());
    }

    @Test
    public void configurePluginLinkIsResolved() throws IOException
    {
        URI configureLink = tab.showSystemPlugins().
            getPlugin(BUNDLED_WITH_CONFIG.getKey()).
            openPluginDetails().
            getConfigureLink().
            getHref();

        assertThat(configureLink.getPath(), endsWith("/admin/plugins/test-plugin-v2-bundled-with-config/configurePlugin.action"));
    }

    @Test
    public void systemPluginsAreNotVisibleByDefault()
    {
        assertFalse(tab.systemPluginsOpen());
    }

    @Test
    public void systemPluginsAreVisibleAfterClickingThroughWarning() throws IOException
    {
        tab.showSystemPlugins();
        assertTrue(tab.systemPluginsOpen());
    }

    @Test
    public void systemPluginsWarningDisplaysShowLinkByDefault()
    {
        assertTrue(tab.hasShowSystemPluginsLink());
    }

    @Test
    public void systemPluginsWarningDoesNotDisplayHideLinkByDefault()
    {
        assertFalse(tab.hasHideSystemPluginsLink());
    }

    @Test
    public void systemPluginsWarningDoesNotDisplayShowAfterClickingShow() throws IOException
    {
        tab.showSystemPlugins();

        assertFalse(tab.hasShowSystemPluginsLink());
    }

    @Test
    public void systemPluginsWarningDisplaysHideAfterClickingShow() throws IOException
    {
        tab.showSystemPlugins();

        assertTrue(tab.hasHideSystemPluginsLink());
    }

    @Test
    public void disabledPluginsHaveAllPluginModulesLookDisabled()
    {
        try
        {
            installPlugin(USER_INSTALLED_WITH_MODULES);
            tab.getPlugin(USER_INSTALLED_WITH_MODULES.getKey()).
                openPluginDetails().
                disable();
            reloadTab();

            assertThat(
                    tab.getPlugin(USER_INSTALLED_WITH_MODULES.getKey()).
                        openPluginDetails().
                        getNumberOfModulesEnabled(),
                    is(equalTo(0)));
        }
        finally
        {
            tab.uninstallPlugin(USER_INSTALLED_WITH_MODULES.getKey());
        }
    }

    @Test
    public void enabledPluginsHaveAllPluginModulesLookEnabled() throws Exception
    {
        try
        {
            installPlugin(USER_INSTALLED_WITH_MODULES);

            assertThat(
                    tab.getPlugin(USER_INSTALLED_WITH_MODULES.getKey()).
                        openPluginDetails().
                        getNumberOfModulesEnabled(),
                    is(equalTo(1)));
        }
        finally
        {
            tab.uninstallPlugin(USER_INSTALLED_WITH_MODULES.getKey());
        }
    }

    @Test
    public void disablingAPluginRefreshesPluginModulesState() throws Exception
    {
        try
        {
            installPlugin(USER_INSTALLED_WITH_MODULES);

            InstalledPluginDetails pluginDetails = tab.getPlugin(USER_INSTALLED_WITH_MODULES.getKey()).openPluginDetails();
            pluginDetails.disable();

            assertThat(pluginDetails.getNumberOfModulesEnabled(), is(equalTo(0)));
        }
        finally
        {
            tab.uninstallPlugin(USER_INSTALLED_WITH_MODULES.getKey());
        }
    }

    @Test
    public void enablingAPluginRefreshesPluginModulesState() throws Exception
    {
        try
        {
            installPlugin(USER_INSTALLED_WITH_MODULES);
            tab.getPlugin(USER_INSTALLED_WITH_MODULES.getKey()).openPluginDetails().disable();
            reloadTab();

            InstalledPluginDetails pluginDetails = tab.getPlugin(USER_INSTALLED_WITH_MODULES.getKey()).openPluginDetails();
            pluginDetails.enable();
            assertThat(pluginDetails.getNumberOfModulesEnabled(), is(equalTo(1)));
        }
        finally
        {
            tab.uninstallPlugin(USER_INSTALLED_WITH_MODULES.getKey());
        }
    }

    @Test
    public void disablingPluginMarksPluginAsDisabled()
    {
        InstalledPlugin plugin = tab.getPlugin(INSTALLABLE.getKey());
        try
        {

            plugin.disable();
            assertTrue(plugin.isDisabled());
        }
        finally
        {
            plugin.enable();
        }
    }

    @Test
    public void disablingPluginShowsSuccessfullyDisabledMessage() throws Exception
    {
        InstalledPlugin plugin = tab.getPlugin(INSTALLABLE.getKey());
        try
        {
            assertTrue(plugin.openPluginDetails().disable().isInfo());
        }
        finally
        {
            if (plugin.isDisabled())
            {
                plugin.enable();
            }
        }
    }

    @Test
    public void verifyInstallRequiresRestartDisplaysAppropriateMessageInPluginDetails() throws Exception
    {
        try
        {
            installPlugin(INSTALLABLE_REQUIRES_RESTART);

            assertTrue(tab.getPlugin(INSTALLABLE_REQUIRES_RESTART.getKey()).openPluginDetails().requiresRestart());
        }
        finally
        {
            upm.cancelChangeRequiringRestart(INSTALLABLE_REQUIRES_RESTART.getKey());
        }
    }

    @Test
    public void verifyUpgradeRequiresRestartDisplaysAppropriateMessageInPluginDetails() throws Exception
    {
        try
        {
            installPlugin(UPGRADEABLE_REQUIRES_RESTART_V2);
            reloadTab();

            tab.showSystemPlugins();
            assertTrue(tab.getPlugin(UPGRADEABLE_REQUIRES_RESTART_V2.getKey()).
                openPluginDetails().
                requiresRestart());
        }
        finally
        {
            upm.cancelChangeRequiringRestart(UPGRADEABLE_REQUIRES_RESTART_V2.getKey());
        }
    }

    @Test
    public void enablingPluginRemovesDisabledClassToPluginDiv() throws Exception
    {
        tab.getPlugin(INSTALLABLE.getKey()).
            openPluginDetails().
            disable();
        reloadTab();

        tab.getPlugin(INSTALLABLE.getKey()).
            openPluginDetails().
            enable();
        assertTrue(tab.getPlugin(INSTALLABLE.getKey()).isEnabled());
    }

    @Test
    public void enablingPluginShowsSuccessfullyEnabledMessage() throws Exception
    {
        tab.getPlugin(INSTALLABLE.getKey()).
            openPluginDetails().
            disable();
        reloadTab();

        assertTrue(tab.getPlugin(INSTALLABLE.getKey()).
            openPluginDetails().
            enable().
            isSuccess());
    }

    @Test
    public void pluginsWithoutModuleShowsNoModuleMessageInPluginDetails() throws Exception
    {
        assertFalse(tab.getPlugin(INSTALLABLE.getKey()).
            openPluginDetails().
            hasModules());
    }

    @Test
    public void pluginsWithModulesShowManagePluginModulesLinkInPluginDetails() throws IOException
    {
        try
        {
            installPlugin(USER_INSTALLED_WITH_MODULES);

            assertTrue(tab.getPlugin(USER_INSTALLED_WITH_MODULES.getKey()).
                    openPluginDetails().
                    hasModules());
        }
        finally
        {
            upm.uninstallPlugin(USER_INSTALLED_WITH_MODULES.getKey());
        }
    }

    @Test
    public void upmPluginActionButtonsAndModuleActionButtonsAreHidden() throws IOException
    {
        tab.showSystemPlugins();
        assertTrue(tab.getPlugin(UPM.getKey()).
            openPluginDetails().
            openModuleManager().
            allPluginModuleActionsAreHidden());
    }

    @Test
    public void nonUpmPluginActionButtonsAndModuleActionButtonsAreVisible() throws Exception
    {
        try
        {
            installPlugin(USER_INSTALLED_WITH_MODULES);

            assertFalse(tab.getPlugin(USER_INSTALLED_WITH_MODULES.getKey()).
                openPluginDetails().
                openModuleManager().
                allPluginModuleActionsAreHidden());
        }
        finally
        {
            tab.uninstallPlugin(USER_INSTALLED_WITH_MODULES.getKey());
        }
    }

    @Test
    public void requiredModuleIsNotAllowedToBeDisabled() throws Exception
    {
        try
        {
            installPlugin(CANNOT_DISABLE_MODULE);

            assertFalse(tab.getPlugin(CANNOT_DISABLE_MODULE.getKey()).
                openPluginDetails().
                openModuleManager().
                getModule("module-cannot-be-disabled").
                canBeDisabled());
        }
        finally
        {
            tab.uninstallPlugin(CANNOT_DISABLE_MODULE.getKey());
        }
    }

    @Test
    public void confirmationDialogIsShownWhenUninstallingPlugin() throws Exception
    {
        ConfirmDialog confirmation = tab.getPlugin(INSTALLABLE.getKey()).
            openPluginDetails().
            uninstall();

        assertTrue(confirmation.canConfirm());
    }

    @Test
    public void uninstallingPluginHidesModules() throws Exception
    {
        installPlugin(USER_INSTALLED_WITH_MODULES);
        InstalledPluginDetails pluginDetails = tab.getPlugin(USER_INSTALLED_WITH_MODULES.getKey()).uninstall();

        assertTrue(pluginDetails.hasModuleStatusCompletelyHidden());
    }
    
    @Test
    public void assertThatAllPluginDetailsAreShownAfterExpandAllInUserInstalledSection() throws Exception
    {
        tab.waitUntilTabIsLoaded();
        tab.expandAllResults(USER_SECTION);
    }
    
    @Test
    public void assertThatAllPluginDetailsAreNotShownAfterCollapseAllInUserInstalledSection() throws Exception
    {
        tab.waitUntilTabIsLoaded();
        tab.expandAllResults(USER_SECTION);
        tab.collapseAllResults(USER_SECTION);
    }
    
    @Test
    public void assertThatAllPluginDetailsAreShownAfterExpandAllInSystemSection() throws Exception
    {
        tab.waitUntilTabIsLoaded();
        tab.showSystemPlugins();
        tab.expandAllResults(SYSTEM_SECTION);
    }
    
    @Test
    public void assertThatAllPluginDetailsAreNotShownAfterCollapseAllInSystemSection() throws Exception
    {
        tab.waitUntilTabIsLoaded();
        tab.showSystemPlugins();
        tab.expandAllResults(SYSTEM_SECTION);
        tab.collapseAllResults(SYSTEM_SECTION);
    }

    private static void installPlugin(TestPlugins testPlugin)
    {
        upm.uploadPlugin(testPlugin.getDownloadUri(getBaseUri()));
        reloadTab();
    }

    private static void reloadTab()
    {
        upm = product.visit(PluginManager.class);
        tab = upm.openManageExistingTab();
    }

    private void withPacBaseUri(URI pacUri, Runnable runnable)
    {
        BasicHttpEntity entity = new BasicHttpEntity();
        entity.setContent(new ByteArrayInputStream(("{ \"pac-base-url\": \"" + pacUri.toASCIIString() + "\" }").getBytes()));
        entity.setContentType("application/vnd.atl.plugins.pac.base.url+json");

        HttpPut put = new HttpPut();
        put.setURI(URI.create(getBaseUri().toASCIIString() + "/rest/plugins/1.0/pac-base-url").normalize());
        put.setEntity(entity);
        put.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials("admin", "admin"), "UTF-8", false));
        DefaultHttpClient client = new DefaultHttpClient();
        try
        {
            HttpResponse response = client.execute(put);
            response.getEntity().consumeContent();
            if (response.getStatusLine().getStatusCode() != 200)
            {
                throw new RuntimeException("Failed to set PAC base URI");
            }
        }
        catch (ClientProtocolException e)
        {
            throw new RuntimeException("Could not set PAC base URI", e);
        }
        catch (IOException e)
        {
            throw new RuntimeException("Could not set PAC base URI", e);
        }
        try
        {
            runnable.run();
        }
        finally
        {
            HttpDelete delete = new HttpDelete(put.getURI());
            delete.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials("admin", "admin"), "UTF-8", false));
            try
            {
                HttpResponse response = client.execute(delete);
                response.getEntity().consumeContent();
                if (response.getStatusLine().getStatusCode() != 200)
                {
                    throw new RuntimeException("Failed to reset PAC base URI");
                }
            }
            catch (ClientProtocolException e)
            {
                throw new RuntimeException("Could reset PAC base URI", e);
            }
            catch (IOException e)
            {
                throw new RuntimeException("Could reset PAC base URI", e);
            }
        }
    }

    private static URI getBaseUri()
    {
        return URI.create(product.getProductInstance().getBaseUrl());
    }
}
