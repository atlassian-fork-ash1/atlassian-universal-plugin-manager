package com.atlassian.upm.rest.resources.upgradeall;

import java.io.File;
import java.net.URI;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

import com.atlassian.plugin.PluginException;
import com.atlassian.plugins.domain.model.plugin.Plugin;
import com.atlassian.plugins.domain.model.plugin.PluginVersion;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.upm.AsyncTaskAwareApplicationProperties;
import com.atlassian.upm.AsyncTaskAwareUserManager;
import com.atlassian.upm.PluginAccessorAndController;
import com.atlassian.upm.PluginDownloadService;
import com.atlassian.upm.PluginDownloadService.ProgressTracker;
import com.atlassian.upm.PluginInstaller;
import com.atlassian.upm.log.AuditLogService;
import com.atlassian.upm.pac.PacClient;
import com.atlassian.upm.rest.UpmUriBuilder;
import com.atlassian.upm.rest.representations.RepresentationFactory;
import com.atlassian.upm.rest.resources.permission.PermissionEnforcer;

import com.google.common.collect.ImmutableList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import static com.atlassian.integrationtesting.ApplicationPropertiesImpl.getStandardApplicationProperties;
import static com.atlassian.upm.test.UpmMatchers.downloadFailedFor;
import static com.atlassian.upm.test.UpmMatchers.upgradeFailureOf;
import static javax.ws.rs.core.Response.Status.CONFLICT;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UpgradeAllResourceTest
{
    @Mock PluginAccessorAndController pluginAccessorAndController;
    SingleThreadedAsynchronousTaskManager taskManager;
    @Mock PacClient pacClient;
    @Mock PluginDownloadService pluginDownloadService;
    @Mock PluginInstaller pluginInstaller;
    @Mock RepresentationFactory representationFactory;
    @Mock AsyncTaskAwareUserManager asyncTaskAwareUserManager;
    @Mock PermissionEnforcer permissionEnforcer;
    @Mock AuditLogService auditLogger;
    @Mock UserManager userManager;
    AsyncTaskAwareApplicationProperties applicationProperties;
    UpmUriBuilder uriBuilder;

    UpgradeAllResource resource;

    private static final String USERNAME = "admin";

    @Before
    public void createUpgradeAllResource()
    {
        when(asyncTaskAwareUserManager.getRemoteUsername()).thenReturn(USERNAME);
        applicationProperties = new AsyncTaskAwareApplicationProperties(getStandardApplicationProperties());
        uriBuilder = new UpmUriBuilder(applicationProperties);
        taskManager = new SingleThreadedAsynchronousTaskManager(uriBuilder, asyncTaskAwareUserManager, applicationProperties);
        resource = new UpgradeAllResource(pluginAccessorAndController, taskManager, pacClient, pluginDownloadService,
            pluginInstaller, representationFactory, permissionEnforcer, auditLogger, uriBuilder, userManager);
        
        when(pluginAccessorAndController.getUpmPluginKey()).thenReturn("arbitrary key that won't be matched");
    }

    @Test
    public void assertThatConflictResponseIsReturnedIfInSafeMode()
    {
        when(pluginAccessorAndController.isSafeMode()).thenReturn(true);

        assertThat(resource.upgradeAll().getStatus(), is(equalTo(CONFLICT.getStatusCode())));
    }

    @Test
    public void verifyThatTaskIsSubmittedWhenNotInSafeMode()
    {
        resource.upgradeAll();

        assertTrue(taskManager.wasTaskSubmitted());
    }

    @Test
    public void assertThatWhileFindingPluginUpgradesTaskStatusIsFindingUpgrades() throws Exception
    {
        final CyclicBarrier barrier = new CyclicBarrier(2);
        whenGettingUpgrades(waitOn(barrier, thenReturn(ImmutableList.<PluginVersion>of())));

        resource.upgradeAll();
        barrier.await(1, TimeUnit.SECONDS);      // wait for first barrier to get hit which means that the process should be in the "finding upgrades" state

        assertThat(
            taskManager.getCurrentlyRunningTask().getRepresentation(uriBuilder).getContentType(),
            is(equalTo(UpgradeStatus.State.FINDING_UPGRADES.getContentType()))
        );
        barrier.await(1, TimeUnit.SECONDS);      // releases the second barrier so that the upgrade thread can complete
    }

    @Test
    public void assertThatWhileDownloadingPluginUpgradesTaskStatusIsDownloadingPlugin() throws Exception
    {
        final CyclicBarrier barrier = new CyclicBarrier(2);
        whenGettingUpgrades(returnUpgrades());
        whenDownloading(waitOn(barrier, thenReturn(new File("/some/file"))));

        resource.upgradeAll();
        barrier.await(1, TimeUnit.SECONDS);      // wait for first barrier to get hit which means that the process should be in the "downloading plugin" state

        assertThat(
            taskManager.getCurrentlyRunningTask().getRepresentation(uriBuilder).getContentType(),
            is(equalTo(UpgradeStatus.State.DOWNLOADING.getContentType()))
        );
        barrier.await(1, TimeUnit.SECONDS);      // releases the second barrier so that the upgrade thread can complete
    }

    @Test
    public void assertThatWhileUpgradingTaskStatusIsUpgradingPlugin() throws Exception
    {
        final CyclicBarrier barrier = new CyclicBarrier(2);
        whenGettingUpgrades(returnUpgrades());
        whenDownloading(returnFile());
        whenUpgrading(waitOn(barrier));

        resource.upgradeAll();
        barrier.await(1, TimeUnit.SECONDS);      // wait for first barrier to get hit which means that the process should be in the "upgrading plugin" state

        assertThat(
            taskManager.getCurrentlyRunningTask().getRepresentation(uriBuilder).getContentType(),
            is(equalTo(UpgradeStatus.State.UPGRADING.getContentType()))
        );
        barrier.await(1, TimeUnit.SECONDS);      // releases the second barrier so that the upgrade thread can complete
    }

    @Test
    public void assertThatDownloadFailuresAreReported() throws Exception
    {
        whenGettingUpgrades(returnUpgrades());

        final CyclicBarrier barrier = new CyclicBarrier(2);
        whenDownloading(waitOn(barrier, throwResponseException())); // we need to pause the exectution so we can grab the task

        resource.upgradeAll();
        barrier.await(1, TimeUnit.SECONDS);    // wait for first barrier to get hit which means that the process should be in the "downloading plugin" state

        UpgradeAllTask task = (UpgradeAllTask) taskManager.getCurrentlyRunningTask();
        barrier.await(1, TimeUnit.SECONDS);    // release the second barrier so that the upgrade thread can complete

        waitForTaskCompletion(task);

        assertThat(task.getResults().getFailures(), contains(downloadFailedFor("Test plugin", "1.1")));
    }

    @Test
    public void assertThatUpgradeFailuresAreReported() throws Exception
    {
        whenGettingUpgrades(returnUpgrades());

        final CyclicBarrier barrier = new CyclicBarrier(2);
        whenDownloading(returnFile());
        whenUpgrading(waitOn(barrier, throwPluginInstallException()));

        resource.upgradeAll();
        barrier.await(1, TimeUnit.SECONDS);    // wait for first barrier to get hit which means that the process should be in the "upgrading" state

        UpgradeAllTask task = (UpgradeAllTask) taskManager.getCurrentlyRunningTask();
        barrier.await(1, TimeUnit.SECONDS);    // release the second barrier so that the upgrade thread can complete

        waitForTaskCompletion(task);

        assertThat(task.getResults().getFailures(), contains(upgradeFailureOf("Test plugin", "1.1")));
    }

    private Answer<Void> waitOn(final CyclicBarrier barrier)
    {
        return waitOn(barrier, this.<Void>thenReturn(null));
    }

    private <T> Answer<T> waitOn(final CyclicBarrier barrier, final Answer<T> answer)
    {
        return new Answer<T>()
        {
            public T answer(InvocationOnMock invocation) throws Throwable
            {
                barrier.await(1, TimeUnit.SECONDS);    // barrier to release the junit thread so it can do the assertion
                // now that it is sure we're in the correct state

                barrier.await(1, TimeUnit.SECONDS);    // wait in the current state until the assertion has been made 
                return answer.answer(invocation);
            }
        };
    }

    private <T> Answer<T> thenReturn(final T returnValue)
    {
        return new Answer<T>()
        {
            public T answer(InvocationOnMock invocation) throws Throwable
            {
                return returnValue;
            }
        };
    }

    private void waitForTaskCompletion(UpgradeAllTask task) throws InterruptedException
    {
        while (!task.getRepresentation(uriBuilder).getStatus().isDone())
        {
            Thread.sleep(100);
        }
    }

    private void whenGettingUpgrades(Answer<ImmutableList<PluginVersion>> answer)
    {
        when(pacClient.getUpgrades()).thenAnswer(answer);
    }

    private Answer<ImmutableList<PluginVersion>> returnUpgrades()
    {
        return new Answer<ImmutableList<PluginVersion>>()
        {
            public ImmutableList<PluginVersion> answer(InvocationOnMock invocation) throws Throwable
            {
                Plugin plugin = new Plugin();
                plugin.setName("Test plugin");
                plugin.setPluginKey("test.plugin");

                PluginVersion pv = new PluginVersion();
                pv.setPlugin(plugin);
                pv.setVersion("1.1");
                pv.setBinaryUrl("http://binary/url");

                return ImmutableList.of(pv);
            }
        };
    }

    private void whenDownloading(Answer<File> answer) throws ResponseException
    {
        when(pluginDownloadService.downloadPlugin(isA(URI.class), anyString(), anyString(), isA(ProgressTracker.class))).thenAnswer(answer);
    }

    private Answer<File> throwResponseException()
    {
        return new Answer<File>()
        {
            public File answer(InvocationOnMock invocation) throws Throwable
            {
                throw new ResponseException("Fake exception");
            }
        };
    }

    private Answer<File> returnFile()
    {
        return new Answer<File>()
        {
            public File answer(InvocationOnMock invocation) throws Throwable
            {
                return new File("/some/file");
            }
        };
    }

    private void whenUpgrading(Answer<Void> answer)
    {
        when(pluginInstaller.upgrade(isA(File.class), isA(String.class))).thenAnswer(answer);
    }


    private Answer<Void> throwPluginInstallException()
    {
        return new Answer<Void>()
        {
            public Void answer(InvocationOnMock invocation) throws Throwable
            {
                throw new PluginException("Fake exception");
            }
        };
    }
}
