package com.atlassian.upm;

import java.io.File;
import java.net.URI;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

import org.springframework.beans.factory.annotation.Qualifier;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Delegates method calls to a dynamically loaded component from the self-upgrade plugin.
 * See {@link SelfUpgradeController}.
 */
public class SelfUpgradePluginAccessorImpl implements SelfUpgradePluginAccessor
{
    // The following constants must be kept in sync with the self-upgrade plugin code -
    // see com.atlassian.upm.selfupgrade.Constants.
    private static final String SELFUPGRADE_SETTINGS_BASE = "com.atlassian.upm:selfupgrade";
    private static final String SELFUPGRADE_SETTINGS_JAR_PATH = SELFUPGRADE_SETTINGS_BASE + ".jar"; 
    private static final String SELFUPGRADE_SETTINGS_UPM_KEY = SELFUPGRADE_SETTINGS_BASE + ".key"; 
    private static final String SELFUPGRADE_SETTINGS_UPM_URI = SELFUPGRADE_SETTINGS_BASE + ".upm.uri"; 
    private static final String SELFUPGRADE_SETTINGS_SELFUPGRADE_PLUGIN_URI = SELFUPGRADE_SETTINGS_BASE + ".stub.uri"; 
    private static final String SELFUPGRADE_EXECUTE_UPDATE_RESOURCE_PATH = "/rest/plugins/self-upgrade/1.0/";
    
    private final ApplicationProperties applicationProperties;
    private final PluginSettingsFactory pluginSettingsFactory;

    public SelfUpgradePluginAccessorImpl(@Qualifier("asyncTaskAwareApplicationProperties") ApplicationProperties applicationProperties,
                                         PluginSettingsFactory pluginSettingsFactory)
    {
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
        this.pluginSettingsFactory = checkNotNull(pluginSettingsFactory, "pluginSettingsFactory");
    }
    
    public URI prepareUpgrade(File jarToInstall, String expectedPluginKey, URI pluginUri, URI selfUpgradePluginUri)
    {
        PluginSettings settings = pluginSettingsFactory.createGlobalSettings();
        settings.put(SELFUPGRADE_SETTINGS_JAR_PATH, jarToInstall.getAbsolutePath());
        settings.put(SELFUPGRADE_SETTINGS_UPM_KEY, expectedPluginKey);
        settings.put(SELFUPGRADE_SETTINGS_UPM_URI, pluginUri.toString());
        settings.put(SELFUPGRADE_SETTINGS_SELFUPGRADE_PLUGIN_URI, selfUpgradePluginUri.toString());
        
        return URI.create(applicationProperties.getBaseUrl() + SELFUPGRADE_EXECUTE_UPDATE_RESOURCE_PATH);
    }
}
