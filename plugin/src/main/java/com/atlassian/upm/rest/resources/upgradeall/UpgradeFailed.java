package com.atlassian.upm.rest.resources.upgradeall;

import com.atlassian.plugins.domain.model.plugin.PluginVersion;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

public final class UpgradeFailed
{
    @JsonProperty private final String name;
    @JsonProperty private final String version;
    @JsonProperty private final String subCode;
    @JsonProperty private final String message;
    @JsonProperty private final String source;
    @JsonIgnore private final UpgradeFailed.Type type;

    @JsonCreator
    public UpgradeFailed(@JsonProperty("type") String type,
        @JsonProperty("name") String name,
        @JsonProperty("version") String version,
        @JsonProperty("subCode") String subCode,
        @JsonProperty("message") String message,
        @JsonProperty("source") String source)
    {
        this(Type.valueOf(type), name, version, subCode, message, source);
    }

    UpgradeFailed(UpgradeFailed.Type type, String subCode, PluginVersion upgrade)
    {
        this(type, subCode, upgrade, null);
    }

    UpgradeFailed(UpgradeFailed.Type type, String subCode, PluginVersion upgrade, String message)
    {
        this(type, upgrade.getPlugin().getName(), upgrade.getVersion(), subCode, message, upgrade.getBinaryUrl());
    }

    private UpgradeFailed(UpgradeFailed.Type type, String name, String version, String subCode, String message, String source)
    {
        this.type = type;
        this.name = name;
        this.version = version;
        this.subCode = subCode;
        this.message = message;
        this.source = source;
    }

    public String getName()
    {
        return name;
    }

    public String getVersion()
    {
        return version;
    }

    public String getSubCode()
    {
        return subCode;
    }

    public String getMessage()
    {
        return message;
    }

    public String getSource()
    {
        return source;
    }

    @JsonProperty
    public UpgradeFailed.Type getType()
    {
        return type;
    }

    public enum Type
    {
        DOWNLOAD, INSTALL
    }
}