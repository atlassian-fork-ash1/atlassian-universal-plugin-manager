package com.atlassian.upm.rest.resources.upgradeall;

import com.atlassian.plugins.domain.model.plugin.PluginVersion;

import org.codehaus.jackson.annotate.JsonProperty;

final class UpgradingPluginStatus extends UpgradeStatus
{
    @JsonProperty private final String name;
    @JsonProperty private final String version;
    @JsonProperty private final int numberComplete;
    @JsonProperty private final int totalUpgrades;

    UpgradingPluginStatus(PluginVersion pluginVersion, int numberComplete, int totalUpgrades)
    {
        super(State.UPGRADING);
        name = pluginVersion.getPlugin().getName();
        version = pluginVersion.getVersion();
        this.numberComplete = numberComplete;
        this.totalUpgrades = totalUpgrades;
    }

    public int getNumberComplete()
    {
        return numberComplete;
    }

    public int getTotalUpgrades()
    {
        return totalUpgrades;
    }

    public String getName()
    {
        return name;
    }

    public String getVersion()
    {
        return version;
    }
}