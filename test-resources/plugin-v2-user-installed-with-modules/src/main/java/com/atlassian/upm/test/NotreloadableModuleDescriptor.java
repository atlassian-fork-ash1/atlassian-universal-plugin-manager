package com.atlassian.upm.test;

import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.descriptors.RequiresRestart;

@RequiresRestart
public class NotreloadableModuleDescriptor extends AbstractModuleDescriptor
{

    @Override
    public Object getModule()
    {
        return null;
    }
}