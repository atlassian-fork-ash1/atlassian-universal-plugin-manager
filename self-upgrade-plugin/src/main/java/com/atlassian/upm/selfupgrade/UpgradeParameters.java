package com.atlassian.upm.selfupgrade;

import java.io.File;
import java.net.URI;

import static com.google.common.base.Preconditions.checkNotNull;

public class UpgradeParameters
{
    private final File jarToInstall;
    private final String expectedPluginKey;
    private final URI pluginUri;
    private final URI selfUpgradePluginUri;
    
    public UpgradeParameters(File jarToInstall, String expectedPluginKey, URI pluginUri, URI selfUpgradePluginUri)
    {
        this.jarToInstall = checkNotNull(jarToInstall, "jarToInstall");
        this.expectedPluginKey = checkNotNull(expectedPluginKey, "expectedPluginKey");
        this.pluginUri = checkNotNull(pluginUri, "pluginUri");
        this.selfUpgradePluginUri = checkNotNull(selfUpgradePluginUri, "selfUpgradePluginUri");
    }

    public File getJarToInstall()
    {
        return jarToInstall;
    }

    public String getExpectedPluginKey()
    {
        return expectedPluginKey;
    }

    public URI getPluginUri()
    {
        return pluginUri;
    }

    public URI getSelfUpgradePluginUri()
    {
        return selfUpgradePluginUri;
    }
}
