package com.atlassian.upm.selfupgrade.async;

import java.net.URI;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicReference;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.executor.ThreadLocalDelegateExecutorFactory;
import com.atlassian.sal.api.user.UserManager;

import static com.atlassian.upm.selfupgrade.Constants.REST_BASE_PATH;
import static com.atlassian.upm.selfupgrade.Constants.REST_TASK_URI_PATH;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Manages the asynchronous execution of at most one task, ever.  By design, this plugin
 * cannot execute its install task more than once.
 */
public class SimpleAsyncTaskManager
{
    private final ApplicationProperties applicationProperties;
    private final ExecutorService executor;
    private final AtomicReference<SimpleAsynchronousTask> currentTask;
    private final UserManager userManager;
    
    public SimpleAsyncTaskManager(ApplicationProperties applicationProperties,
                                  ThreadLocalDelegateExecutorFactory executorFactory,
                                  UserManager userManager)
    {
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
        this.userManager = checkNotNull(userManager, "userManager");
        ExecutorService delegateExecutor = Executors.newFixedThreadPool(1);
        this.executor = checkNotNull(executorFactory, "executorFactory").createExecutorService(delegateExecutor);
        this.currentTask = new AtomicReference<SimpleAsynchronousTask>();
    }
    
    public SimpleAsynchronousTask getTask()
    {
        return currentTask.get();
    }
    
    public SimpleAsynchronousTask start(Callable<TaskStatus> handler, String type, String source, int pollDelay)
    {
        URI taskUri = URI.create(applicationProperties.getBaseUrl() + REST_BASE_PATH + REST_TASK_URI_PATH);
        String username = userManager.getRemoteUsername();
        SimpleAsynchronousTask task = new SimpleAsynchronousTask(handler, type, source, pollDelay, taskUri, username);
        if (!currentTask.compareAndSet(null, task))
        {
            throw new IllegalStateException("Cannot start a task because one has already been started");
        }
        executor.execute(task);
        return task;
    }
}
